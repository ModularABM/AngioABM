# How to contribute

## Working with the repo
- create a branch for each feature/bug you work on
- close the branch when done by merging  with '--no-ff'
- commits must compile!
- commits must return reasonable results
- squash small commits

## commit msg style
- Use the present tense ("Add feature" not "Added feature")
- Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
- Limit the first line to 72 characters or less
- Reference issues and pull requests liberally
- [https://github.com/erlang/otp/wiki/Writing-good-commit-messages]

## coding stlyes
- general
 - comment!
- c++
 - use clang-format
 - use underscore: 'some_name'
- R
 - use ESS
 - use dot: 'some.name'

## code review
- comment oyur code thorouglhy so it can be reviewed
- try to tell others nicely where they could improve their code
- try to regularly review some of your peers code
- from [https://smartbear.com/learn/code-review/best-practices-for-peer-code-review/] there are a few tips:
-- Review fewer than 400 lines of code at a time
-- Take your time. Inspection rates should be under 500 LOC per hour
-- Do not review for more than 60 minutes at a time

## documenting
use markown, e.g. [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#links]
### Text

- Split up long lines, this makes it much easier to review and edit. Only
  double line breaks are shown as a full line break in [GitLab markdown][gfm].
  80-100 characters is a good line length
- Make sure that the documentation is added in the correct directory and that
  there's a link to it somewhere useful
- Do not duplicate information
- Be brief and clear
- Unless there's a logical reason not to, add documents in alphabetical order
- Write in US English
- Use [single spaces][] instead of double spaces

### Formatting

- Use dashes (`-`) for unordered lists instead of asterisks (`*`)
- Use the number one (`1`) for ordered lists
- Use underscores (`_`) to mark a word or text in italics
- Use double asterisks (`**`) to mark a word or text in bold
- When using lists, prefer not to end each item with a period. You can use
  them if there are multiple sentences, just keep the last sentence without
  a period

### Headings

- Add only one H1 title in each document, by adding `#` at the beginning of
  it (when using markdown). For subheadings, use `##`, `###` and so on
- Avoid putting numbers in headings. Numbers shift, hence documentation anchor
  links shift too, which eventually leads to dead links. If you think it is
  compelling to add numbers in headings, make sure to at least discuss it with
  someone in the Merge Request
- Leave exactly one newline after a heading

