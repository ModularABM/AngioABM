#include "DiffEqEC_agent.h"

// to throw exceptions
struct missing_variable {
  std::string varname;

  missing_variable() { varname = "NA"; };
  missing_variable(std::string varname_value) { varname = varname_value; };
};


// constructor
DiffEqEC_agent::DiffEqEC_agent(int x_val, int y_val, int id_val,
                               std::unordered_map<std::string, double> *v_val,
                               std::unordered_map<std::string, double> *p_val)
    : Agent(x_val, y_val, id_val, "DiffEqECagent", v_val, p_val) {
  type = "DiffEqEC_agent";

  // safety check: are all variables included?
  std::vector<std::string> v_names = {
      "actin",       "dll4_mRNA", "dll4",       "vegfr2_mRNA",
      "vegfr1_mRNA", "vegfr2",    "vegfr2_act", "mvegfr1",
      "svegfr1",     "notch_act", "tip",        "branching"};
  for(unsigned i = 0; i < v_names.size(); ++i) {

    if(v.find(v_names[i]) == v.end()) {

      throw missing_variable(v_names[i]);
    }
  }

  // safety check: are all parameters included?
  std::vector<std::string> p_names = {"notch0",
                                      "k1",
                                      "k2",
                                      "k3",
                                      "k4",
                                      "k5",
                                      "k6dll4",
                                      "k6vegfr1",
                                      "k6vegfr2",
                                      "k9",
                                      "uptake_factor",
                                      "k10",
                                      "k11",
                                      "k12",
                                      "k13",
                                      "k15",
                                      "k16",
                                      "k17",
                                      "k18",
                                      "k19",
                                      "V_max",
                                      "A_star",
                                      "filofactor"};
  for(unsigned i = 0; i < p_names.size(); ++i) {

    if(p.find(p_names[i]) == p.end()) {

      throw missing_variable(p_names[i]);
    }
  }
};


// update internal states (and environment)
void DiffEqEC_agent::update(Globalsvector &globals, Localsvector2d &locals,
                            Agentvector2d &oldstate, Agentvector2d &newstate,
                            double timepoint, int t_scale, int t_intstep,
                            std::mt19937 &rng) {


  // sense environment
  // double vegf_here = sense_nb_vegf(locals, x, y);
  int s_range = static_cast<int>(v["actin"] / p["filofactor"] +
                                 1);  // actin represents lamellopodial length
  double vegf_here = sense_nb_localvar(locals, "vegf", x, y, s_range);
  double vegf_nb   = sense_nb_localvar(locals, "vegf", x, y, 1);


  // determine rate eqs:
  double v_actin_degradation = (p["k10"] * v["actin"]) / t_scale;
  double v_actin_by_vegfr    = (p["k9"] * pow(v["vegfr2_act"], 4) /
                             (pow(v["vegfr2_act"], 4) + pow(p["k19"], 4))) /
                            t_scale;
  double v_dll4_degradation      = (p["k6dll4"] * v["dll4"]) / t_scale;
  double v_dll4_mRNA_degradation = (p["k4"] * v["dll4_mRNA"]) / t_scale;
  double v_dll4_transcription    = (p["k3"] * pow(v["vegfr2_act"], 4) /
                                 (pow(v["vegfr2_act"], 4) + pow(p["k11"], 4))) /
                                t_scale;
  double v_dll4_translation = (p["k5"] * v["dll4_mRNA"]) / t_scale;
  double v_notch_activation = (sense_nb_variable(oldstate, "dll4") *
                               (p["notch0"] - v["notch_act"]) * p["k1"]) /
                              t_scale;
  double v_notch_inactivation = (p["k2"] * v["notch_act"]) / t_scale;
  double v_vegfr2_activation  = (p["k12"] * v["vegfr2"] * pow(vegf_here, 2) /
                                (pow(vegf_here, 2) + pow(p["k13"], 2))) /
                               t_scale;
  double v_vegfr2_act_degradation = (p["k6vegfr2"] * v["vegfr2_act"]) / t_scale;
  double v_vegfr2_degradation     = (p["k6vegfr2"] * v["vegfr2"]) / t_scale;
  double v_vegfr2_translation     = (p["k5"] * v["vegfr2_mRNA"]) / t_scale;
  double v_vegfr2_mRNA_degradation = (p["k4"] * v["vegfr2_mRNA"]) / t_scale;
  double v_vegfr2_transcription =
      (p["k3"] *
       (1 -
        pow(v["notch_act"], 4) / (pow(v["notch_act"], 4) + pow(p["k16"], 4)))) /
      t_scale;
  double v_vegfr1_transcription =
      (p["k3"] * pow(v["notch_act"], 4) /
       (pow(v["notch_act"], 4) + pow(p["k16"], 4))) /
      t_scale;
  double v_vegfr1_mRNA_degradation = (p["k4"] * v["vegfr1_mRNA"]) / t_scale;
  double v_svegfr1_translation = (p["k5"] * v["vegfr1_mRNA"] * 0.5) / t_scale;
  double v_mvegfr1_translation = (p["k5"] * v["vegfr1_mRNA"] * 0.5) / t_scale;
  double v_mvegfr1_degradation = (p["k6vegfr1"] * v["mvegfr1"]) / t_scale;
  double v_svegfr1_degradation = (p["k6vegfr1"] * v["svegfr1"]) / t_scale;
  double v_svegfr1_secretion   = (p["k17"] * v["svegfr1"]) / t_scale;
  double v_vegfr2_act_inactivation = (p["k18"] * v["vegfr2_act"]) / t_scale;

  // modify environment
  double vegf_uptake_by_vegfr2 = v_vegfr2_activation;
  double vegf_uptake_by_vegfr1 =
      (p["k12"] * v["mvegfr1"] * vegf_nb / (vegf_nb + p["k13"])) / t_scale;

  if(vegf_uptake_by_vegfr2 > 0) {
    int rem_range = static_cast<int>(v["actin"] / p["filofactor"] + 2);
    rem_nb_localvar(locals, "vegf", x, y,
                    vegf_uptake_by_vegfr2 / p["uptake_factor"], rem_range);
  }

  if(vegf_uptake_by_vegfr1 > 0) {
    int rem_range = 1;  //  actin/1000 + 2;
    rem_nb_localvar(locals, "vegf", x, y,
                    vegf_uptake_by_vegfr1 / p["uptake_factor"], rem_range);
  }

  if(v_svegfr1_secretion > 0) {
    secrete_nb_localvar(locals, "svegfr1", x, y,
                        v_svegfr1_secretion / p["uptake_factor"]);
  }


  // update internal variables
  v["vegfr2_mRNA"] = std::max(0.0,
                              v["vegfr2_mRNA"] + v_vegfr2_transcription -
                                  v_vegfr2_mRNA_degradation);

  v["vegfr1_mRNA"] = std::max(0.0,
                              v["vegfr1_mRNA"] + v_vegfr1_transcription -
                                  v_vegfr1_mRNA_degradation);

  v["dll4_mRNA"] = std::max(
      0.0, v["dll4_mRNA"] + v_dll4_transcription - v_dll4_mRNA_degradation);

  v["dll4"] =
      std::max(0.0, v["dll4"] + v_dll4_translation - v_dll4_degradation);

  v["vegfr2"] =
      std::max(0.0,
               v["vegfr2"] + v_vegfr2_translation - v_vegfr2_activation -
                   v_vegfr2_degradation + v_vegfr2_act_inactivation);

  v["vegfr2_act"] =
      std::max(0.0,
               v["vegfr2_act"] + v_vegfr2_activation -
                   v_vegfr2_act_degradation - v_vegfr2_act_inactivation);

  v["mvegfr1"] = std::max(0.0,
                          v["mvegfr1"] + v_mvegfr1_translation -
                              v_mvegfr1_degradation - vegf_uptake_by_vegfr1);

  v["svegfr1"] = std::max(0.0,
                          v["svegfr1"] + v_svegfr1_translation -
                              v_svegfr1_degradation - v_svegfr1_secretion);

  v["notch_act"] = std::max(0.0, std::min(p["notch0"],
                                          v["notch_act"] + v_notch_activation -
                                              v_notch_inactivation));

  v["actin"] =
      std::max(0.0, v["actin"] + v_actin_by_vegfr - v_actin_degradation);

  v["tip"] = v["vegfr2_mRNA"] > v["vegfr1_mRNA"] and
                     v["actin"] > p["A_star"] and v["dll4_mRNA"] > p["V_max"]
                 ? 1.0
                 : 0.0;


  // move towards gradient if tip
  if(v["tip"] == 1.0 && t_intstep == 0) {

    // read out gradient (find patch with maximum vegf value in s_range)
    int s_range = static_cast<int>(v["actin"] / p["filofactor"] +
                                   1);  // actin represents lamellopodial length

    std::pair<int, int> to_patch =
        turn_up_gradient(locals, "vegf", s_range, rng);

    std::pair<int, int> oldpos = {x, y};
    std::vector<int> oldneighbours = neighbours;

    // check for non-tip neighbours that could proliferate and fill the empty
    // space
    std::vector<int> nontip_EC_neighbours;
    for(auto it = oldneighbours.begin(); it != oldneighbours.end(); ++it) {

      Agent *curr_agent = newstate.agents[*it];
      if(DiffEqEC_agent *curr_magent =
             dynamic_cast<DiffEqEC_agent *>(curr_agent)) {

        if(curr_magent->v["tip"] == 0.0) {

          nontip_EC_neighbours.push_back(*it);
        }
      }
    }
    int did_move = 1;

    // can only move when there is a stalk cell to fill the emptyt spot
    if(nontip_EC_neighbours.size() > 0) {

      // use a little wiggling here already
      int x_step = to_patch.first;
      int y_step = to_patch.second;

      // try wiggling left and right randomly
      std::pair<int, int> wiggle_aclock = turn(x_step, y_step, 0);
      std::pair<int, int> wiggle_clock  = turn(x_step, y_step, 1);

      std::vector<std::pair<int, int>> new_dirs = {
          wiggle_aclock, wiggle_clock, to_patch, to_patch, to_patch};
      shuffle(new_dirs.begin(), new_dirs.end(), rng);

      // did_move = newstate.move_agent(id, to_patch.first, to_patch.second);
      did_move = newstate.move_agent(id, new_dirs[0].first, new_dirs[0].second);
      new_dirs.erase(new_dirs.begin());
      while(did_move == 1 and new_dirs.size() > 0) {

        did_move =
            newstate.move_agent(id, new_dirs[0].first, new_dirs[0].second);
        new_dirs.erase(new_dirs.begin());
      }

      // try wiggling randomly
      int counter = 0;
      while((did_move == 1) & (counter < 10)) {
        std::pair<int, int> rand_dir = wiggle();
        did_move = newstate.move_agent(id, rand_dir.first, rand_dir.second);
        counter++;
      }

      // insert new stalk agent if movement was succesful
      if(did_move == 0) {

        long unsigned random_nb_idx = rng() % (int)nontip_EC_neighbours.size();
        int random_nb               = nontip_EC_neighbours[random_nb_idx];
        DiffEqEC_agent *curr_agent =
            dynamic_cast<DiffEqEC_agent *>(newstate.agents[random_nb]);
        DiffEqEC_agent new_stalkcell = *curr_agent;

        // update branching information
        new_stalkcell.v["branching"] = 1;
        v["branching"]               = 2;

        newstate.add_agent_kid(oldpos.first, oldpos.second, new_stalkcell);
      }
    }
  }
};

void DiffEqEC_agent::initialize(Globalsvector &globals, Localsvector2d &locals,
                                Agentvector2d &agents, double t, int t_scale) {
  // throw error message if locals has no vegf
  if(locals.contents.count("vegf") < 1) {
    throw missing_variable("vegf");
  }
};

// // sense dll4 in neighbouring agents
// double DiffEqEC_agent::sense_nb_dll4(Agentvector2d agentset) {

//   double dll4 = 0.0;

//   for(vector<int>::size_type i = 0; i != neighbours.size(); i++) {

//     int nb_id         = neighbours[i];
//     Agent *curr_agent = agentset.agents[nb_id];

//     if(DiffEqEC_agent *curr_magent =
//            dynamic_cast<DiffEqEC_agent *>(curr_agent)) {

//       int num_nbs = (int)curr_magent->neighbours.size();
//       dll4 += curr_magent->dll4 / num_nbs;
//     }
//   }
//   return dll4;
// };


// // get notch from neighbouring cells (which secrete equally to all
// neighbours)
// double DiffEqEC_agent::sense_nb_notch(Agentvector2d agentset) {

//   double notch = 0.0;

//   for(vector<int>::size_type i = 0; i != neighbours.size(); i++) {

//     int nb_id         = neighbours[i];
//     Agent *curr_agent = agentset.agents[nb_id];

//     if(DiffEqEC_agent *curr_magent =
//            dynamic_cast<DiffEqEC_agent *>(curr_agent)) {
//       int num_nbs = (int)curr_magent->neighbours.size();
//       notch += curr_magent->notch_act / num_nbs;
//     }
//   }
//   return notch;
// };
