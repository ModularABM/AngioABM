////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef AGENTTYPES_MECHEC_AGENT_H
#define AGENTTYPES_MECHEC_AGENT_H

#include <algorithm>    // using, at least, std::max_element
#include <random>
#include <ctime>
#include <unordered_map>
#include "sys/time.h"

#include "../core/Agent.h"

// class of EC-Agents for the FOR2165 project:
// starting as non-tip cells, this can initially turn into a tip cell and
// has update/movement rules as specified on
// https://gitlab.com/FOR2165/3Dmodels/wikis/model0

class MechEC_agent : public Agent {
public:
  // ___________________________________________________________________________
  // constructors:
  MechEC_agent(int x_val, int y_val, int id_val,
                 std::unordered_map<std::string, double> *v_val,
                 std::unordered_map<std::string, double> *p_val);


  // ___________________________________________________________________________
  // member functions:


  void update(Globalsvector &globalvars, Localsvector2d &localvars,
              Agentvector2d &oldstate, Agentvector2d &newstate, double t,
              int t_scale, int t_intstep, std::mt19937 &rng);

  void initialize(Globalsvector &globals, Localsvector2d &locals,
                          Agentvector2d &agents, double t, int t_scale);


  // apply forces (after movement)
  void apply_forces(Localsvector2d &locals, Agentvector2d &agents);

  // remove forces (after movement)
  void remove_forces(Localsvector2d &locals, Agentvector2d &agents, std::pair<int, int> pos);


};

#endif
