#include "MechEC_agent.h"

// to throw exceptions
struct missing_variable {
  std::string varname;

  missing_variable() { varname = "NA"; };
  missing_variable(std::string varname_value) { varname = varname_value; };
};


// constructor
MechEC_agent::MechEC_agent(int x_val, int y_val, int id_val,
                           std::unordered_map<std::string, double> *v_val,
                           std::unordered_map<std::string, double> *p_val)
    : Agent(x_val, y_val, id_val, "MechECagent", v_val, p_val) {
  type = "MechEC_agent";

  // safety check: are all variables included?
  std::vector<std::string> v_names = {
      "tip",   // tip cell?
      "Ceps",  // deformaiton in cells neighborhood
      "xdir",  // orientation in x-direction
      "ydir",  // orientation in y-direction
      "f01",   // force exerted on neighborfield 0
      "f0-1",  // force exerted on neighborfield 1
      "f11",   // force exerted on neighborfield 2
      "f10",   // force exerted on neighborfield 3
      "f1-1",  // force exerted on neighborfield 4
      "f-11",  // force exerted on neighborfield 5
      "f-10",  // force exerted on neighborfield 6
      "f-1-1"  // force exerted on neighborfield 7
  };
  // neighborfields are numbered by concatening x,y steps from origin

  for(unsigned i = 0; i < v_names.size(); ++i) {

    if(v.find(v_names[i]) == v.end()) {

      throw missing_variable(v_names[i]);
    }
  }

  // safety check: are all parameters included?
  std::vector<std::string> p_names = {
      "FTt",    // tip cells traction force
      "pN",     // probability of tip -> non-tip (percentage)
      "FTn",    // non-tip cells traction force
      "pT",     // probability non-tip -> tip (percentage)
      "FCtn",   // contact force tip to non-tip
      "FCnn",   // contact force non-tip to non-tip
      "FCtt"};  // contact force tip to tip

  for(unsigned i = 0; i < p_names.size(); ++i) {

    if(p.find(p_names[i]) == p.end()) {

      throw missing_variable(p_names[i]);
    }
  }
};


// update internal states (and environment)
void MechEC_agent::update(Globalsvector &globals, Localsvector2d &locals,
                          Agentvector2d &oldstate, Agentvector2d &newstate,
                          double timepoint, int t_scale, int t_intstep,
                          std::mt19937 &rng) {
  // std::cout << "MechEC_agent:update " << id << " @" << timepoint <<
  // std::endl;
  if(t_intstep == 0) {


    // check for non-tip and tip neighbours
    std::vector<int> oldneighbours = neighbours;
    std::vector<int> nontip_EC_neighbours;
    std::vector<int> tip_EC_neighbours;
    for(auto it = oldneighbours.begin(); it != oldneighbours.end(); ++it) {

      Agent *curr_agent = newstate.agents[*it];
      if(MechEC_agent *curr_magent = dynamic_cast<MechEC_agent *>(curr_agent)) {

        if(curr_magent->v["tip"] == 0.0) {

          nontip_EC_neighbours.push_back(*it);
        } else {
          tip_EC_neighbours.push_back(*it);
        }
      }
    }

    // std::cout << "\tMechEC_agent: roll dice" << std::endl;
    // roll a dice to become a tip cell
    std::pair<int, int> currpos = {x, y};
    remove_forces(locals, newstate, currpos);
    std::uniform_real_distribution<> dis(0, 100);
    double tipdice = dis(rng);
    if(v["tip"] == 1) {
      if(tipdice < p["pN"]) {
        // std::cout << "tip " << id << " to stalk " << x << "," << y
        //           << ", currpos is " << currpos.first << "," <<
        //           currpos.second
        //           << std::endl;

        // remove_forces(locals, newstate, currpos);
        v["tip"] = 0;
        // remove/apply forces here to update
        // apply_forces(locals, newstate);
      }
    } else if(tip_EC_neighbours.size() == 0) {
      if(tipdice < p["pT"]) {
        // std::pair<int, int> currpos = {x, y};
        // std::cout << "stalk " << id << " to tip " << x << "," << y
        //           << ", currpos is " << currpos.first << "," <<
        //           currpos.second
        //           << std::endl;
        // remove_forces(locals, newstate, currpos);
        v["tip"] = 1;
        // remove/apply forces here to update
        // apply_forces(locals, newstate);
      }
    }
    apply_forces(locals, newstate);
    // std::cout << "\tMechEC_agent: update Ceps" << std::endl;
    // update Ceps HERE!
    // std::cout << "sensing Eps in neighoburhood \n";
    v["Ceps"] = sense_nb_localvar(locals, "Eps", x, y, 1);
    // std::cout << "after applying forces, v[Ceps] is " << v["Ceps"] << " \n";

    // std::cout << "\tMechEC_agent: get ffields" << std::endl;
    // do movement and check if it improves (and copy neighbor in the process)
    // get fields where forces applpied
    // vector<uint> ffields;
    // for(uint i = 0; i < 8; i++) {
    //   if(v["f" + std::to_string(i)] > 0) {
    //     ffields.push_back(i);
    //   }
    // }
    std::vector<std::pair<int, int>> ffields = {};
    for(int x_step = -1; x_step < 2; x_step++) {
      for(int y_step = -1; y_step < 2; y_step++) {
        if(v["f" + std::to_string(x_step) + std::to_string(y_step)] > 0) {
          ffields.push_back(std::pair<int, int>(x_step, y_step));
        }
      }
    }


    // std::cout << "\tMechEC_agent:sample target field " << std::endl;
    // sample target field based on forces applied and get the direction
    long unsigned randpos = rng();
    int tf_xdir           = 0;
    int tf_ydir           = 0;
    // std::cout << "\tMechEC_agent: smapled " << randpos << ", now compute
    // modulo"
    //          << std::endl;
    // std::cout << "\t\tby " << ffields.size() << std::endl;
    if(ffields.size() > 0) {
      randpos = static_cast<unsigned>(randpos % ffields.size());
      // std::cout << "\tMechEC_agent: and it is " << randpos << std::endl;
      // std::pair<int, int> tf = ffields[randpos];  // rng() % ffields.size()];
      // std::cout << "MechEC_agent: got tf" << std::endl;
      // std::cout << "MechEC_agent: and its first position is "
      //           << ffields[randpos].first << std::endl;
      tf_xdir = ffields[randpos].first;
      tf_ydir = ffields[randpos].second;
    } else {  // if there are no fields that the cell applied forces to:
      // apply forces:
      apply_forces(locals, newstate);
      for(int x_step = -1; x_step < 2; x_step++) {
        for(int y_step = -1; y_step < 2; y_step++) {
          if(v["f" + std::to_string(x_step) + std::to_string(y_step)] > 0) {
            ffields.push_back(std::pair<int, int>(x_step, y_step));
          }
        }
      }

      // std::cout << "\t\tapplied forces, so now I have" << std::endl;
      // std::cout << "\t\tffields.size =  " << ffields.size() << std::endl;

      // move randomly
      // randpos = randpos % 8;
      // tf_xdir = (rng() % 3) - 1;
      // tf_ydir = (rng() % 3) - 1;
    }


    std::pair<int, int> tf_coords(x + tf_xdir, y + tf_ydir);


    // std::cout << "\tMechEC_agent: check Eps at new pos" << std::endl;
    // check deformation at new target
    double tf_Eps =
        sense_nb_localvar(locals, "Eps", tf_coords.first, tf_coords.second, 1);

    std::pair<int, int> oldpos = {x, y};
    int did_move = 1;  // 1 until moved, then 0

    // std::cout << "\tMechEC_agent: check for non-tip neighbours, tot nbs is"
    //          << oldneighbours.size() << std::endl;

    // std::cout << "\tMechEC_agent: do the movement with "
    //    << nontip_EC_neighbours.size() << " neighbors" << std::endl;
    // check movement conditions and move
    if(v["tip"] == 1 && t_intstep == 0) {
      if(tf_Eps + (abs(tf_xdir) * 2 + abs(tf_ydir) * 2 + 2) <= v["Ceps"]) {
        // std::cout << "\t\tMechEC_agent: tf < v[Ceps]" << std::endl;

        // move and copy neighbour to old pos
        did_move = newstate.move_agent(id, static_cast<int>(v["xdir"]),
                                       static_cast<int>(v["ydir"]));
        // std::cout << "\t\tMechEC_agent: did move:" << did_move << std::endl;

        if(did_move == 0) {
          // std::cout << "tip " << id << " moved to " << x << "," << y
          //           << ", oldpos was " << oldpos.first << "," <<
          //           oldpos.second
          //           << std::endl;
          remove_forces(locals, newstate, oldpos);

          // std::cout << "\t\tMechEC_agent: if did_move is 0" << std::endl;
          // copy neighbor to old position
          if(nontip_EC_neighbours.size() > 0) {
            long unsigned random_nb_idx =
                rng() % (int)nontip_EC_neighbours.size();
            int random_nb = nontip_EC_neighbours[random_nb_idx];

            MechEC_agent *curr_agent =
                dynamic_cast<MechEC_agent *>(newstate.agents[random_nb]);
            if(curr_agent->v["tip"] == 0) {
              MechEC_agent new_stalkcell = *curr_agent;
              // std::cout << "\t\tMechEC_agent: created new nb" << std::endl;
              newstate.add_agent_kid(oldpos.first, oldpos.second,
                                     new_stalkcell);
              // std::cout << "\t\tMechEC_agent: inserted new nb" << std::endl;
              // std::cout << "\t\tMechEC_agent: tip inserted new nb "
              //           << new_stalkcell.id << std::endl;

              // set new forces HERE!
              MechEC_agent *inserted_agent = dynamic_cast<MechEC_agent *>(
                  newstate.coords[oldpos.first][oldpos.second]);
              inserted_agent->apply_forces(locals, newstate);
            }
            // std::cout << "\t\tMechEC_agent: set new forces for new nb"
            //           << std::endl;
          }
        }
      }

    } else if(t_intstep == 0) {
      // std::cout << "\tMechEC_agent: do the movement for non-tip" <<
      // std::endl;

      if(tf_Eps + (abs(tf_xdir) * 2 + abs(tf_ydir) * 2 + 2) >= v["Ceps"]) {
        // std::cout << "\t\tMechEC_agent: tf < v[Ceps]" << std::endl;
        // move and copy neighbor to old pos IF no neighbours anymore
        did_move = newstate.move_agent(id, static_cast<int>(v["xdir"]),
                                       static_cast<int>(v["ydir"]));
        // std::cout << "\t\tMechEC_agent: did move:" << did_move << std::endl;

        if(did_move == 0) {
          // std::cout << "stalk " << id << " moved to " << x << "," << y
          //           << ", oldpos was " << oldpos.first << "," <<
          //           oldpos.second
          //           << std::endl;
          remove_forces(locals, newstate, oldpos);
          // std::cout << "\t\tMechEC_agent: if did_move is 0" << std::endl;
          // copy neighbor to old position
          // std::cout << "\t\tMechEC_agent: has " <<
          // nontip_EC_neighbours.size()
          //          << " neighbors" << std::endl;
          // if(nontip_EC_neighbours.size() > 0) {

          //   long unsigned random_nb_idx =
          //       rng() % (int)nontip_EC_neighbours.size();
          //   int random_nb = nontip_EC_neighbours[random_nb_idx];
          //   MechEC_agent *curr_agent =
          //       dynamic_cast<MechEC_agent *>(newstate.agents[random_nb]);
          //   MechEC_agent new_stalkcell = *curr_agent;
          //   // std::cout << "\t\tMechEC_agent: created new nb" << std::endl;
          //   newstate.add_agent_kid(oldpos.first, oldpos.second,
          //   new_stalkcell);
          //   // std::cout << "\t\tMechEC_agent: stalk inserted new nb "
          //   //           << new_stalkcell.id << std::endl;
          //   // set new forces HERE!
          //   MechEC_agent *inserted_agent = dynamic_cast<MechEC_agent *>(
          //       newstate.coords[oldpos.first][oldpos.second]);
          //   inserted_agent->apply_forces(locals, newstate);
          // std::cout << "\t\tMechEC_agent: set new forces for new nb"
          //           << std::endl;
        }
      }
    }


    // std::cout << "\tMechEC_agent: done moving, updating direction and Ceps"
    //          << std::endl;
    if(did_move == 0) {
      // std::cout << "\tMechEC_agent: in if at the end" << std::endl;
      // update direction
      v["xdir"] = tf_xdir;
      v["ydir"] = tf_ydir;

      // clean up old forces
      // remove_forces(locals, newstate, oldpos);
      // set new forces HERE!
      apply_forces(locals, newstate);

      // update Ceps
      v["Ceps"] = tf_Eps;
      // std::cout << "after moving, v[Ceps] is " << v["Ceps"] << " \n";
    }
  }
  // std::cout << "eventually, v[Ceps] is " << v["Ceps"] << " \n";
};

void MechEC_agent::initialize(Globalsvector &globals, Localsvector2d &locals,
                              Agentvector2d &agents, double t, int t_scale) {
  // throw errors when the necessary locals are not there
  if(locals.contents.count("FTx") < 1) {
    throw missing_variable("FTx");
  }
  if(locals.contents.count("FTy") < 1) {
    throw missing_variable("FTy");
  }
  if(locals.contents.count("Eps") < 1) {
    throw missing_variable("Eps");
  }
};


// apply forces (after movement)
void MechEC_agent::apply_forces(Localsvector2d &locals,
                                Agentvector2d &newstate) {
  int idim       = newstate.xy_max.first;
  int jdim       = newstate.xy_max.second;
  double force_v = 0;
  if(v["tip"] == 1) {
    force_v = p["FTt"];
  } else if(v["tip"] == 0) {
    force_v = p["FTn"];
  }
  // check the fields according to the direction if they are free, apply force
  for(int x_step = -1; x_step < 2; x_step++) {
    for(int y_step = -1; y_step < 2; y_step++) {

      // reset agents' account of exerting force on fields
      std::string Fcurr_pos =
          "f" + std::to_string(x_step) + std::to_string(y_step);
      v[Fcurr_pos] = 0;

      // skip if orthogonal to cells direction
      if(v["tip"] == 1) {
        if(v["xdir"] == x_step && v["ydir"] == y_step) {
          continue;
        } else if(v["xdir"] == -x_step && v["ydir"] == -y_step) {
          continue;
        }
      }

      // get coordinates based on cells current position
      int curr_dir_x = x + x_step;
      int curr_dir_y = y + y_step;
      if(curr_dir_x >= idim || curr_dir_x < 0 || curr_dir_y >= jdim ||
         curr_dir_y < 0) {
        continue;
      }

      // std::cout << "\tapplying forces, checking free of " << curr_dir_x
      //           << ", " << curr_dir_y << std::endl;
      // add up vector components
      if(newstate.check_free(curr_dir_x, curr_dir_y)) {
        // double old_val =
        //     locals.contents["Eps"]->values[curr_dir_x][curr_dir_y];
        if(pow(x_step, 2) - pow(y_step, 2) == 0) {
          locals.contents["FTx"]->values[curr_dir_x][curr_dir_y] +=
              -1 * force_v * 1 / std::sqrt(2) * x_step;
          locals.contents["FTy"]->values[curr_dir_x][curr_dir_y] +=
              -1 * force_v * 1 / std::sqrt(2) * y_step;
        } else {
          locals.contents["FTx"]->values[curr_dir_x][curr_dir_y] +=
              -1 * force_v * x_step;
          locals.contents["FTy"]->values[curr_dir_x][curr_dir_y] +=
              -1 * force_v * y_step;
        }
        // if(locals.contents["Eps"]->values[curr_dir_x][curr_dir_y] < 0) {
        //   std::cout << "apply_forces,t=1," << id
        //             << ": Eps is already negative:"
        //             <<
        //             locals.contents["Eps"]->values[curr_dir_x][curr_dir_y]
        //             << std::endl;
        // }
        locals.contents["Eps"]->values[curr_dir_x][curr_dir_y] +=
            force_v;  // force_v / std::sqrt(pow(x_step, 2) + pow(y_step, 2));
        // if(old_val ==
        //    locals.contents["Eps"]->values[curr_dir_x][curr_dir_y]) {
        //   std::cout << "applied force " << val << " but locals didn;t
        //   change"
        //             << std::endl;
        // }
        // if(locals.contents["Eps"]->values[curr_dir_x][curr_dir_y] < 0) {
        // std::cout << "apply_forces,t=1," << id << ": Eps is now negative:"
        //           << locals.contents["Eps"]->values[curr_dir_x][curr_dir_y]
        //           << ", val is " << val << std::endl;
        // }

        v[Fcurr_pos] = force_v;
        // val;  // force_v / std::sqrt(pow(x_step, 2) + pow(y_step, 2));
        // std::cout << "MECa::apply_forces: jsut reset v[" << Fcurr_pos
        //           << "] to "
        //           << force_v / std::sqrt(pow(x_step, 2) + pow(y_step, 2))
        //           << " because v[FTt]=" << force_v << std::endl;
      }
      // std::cout << "\tapplying forces, checked free of " << curr_dir_x <<
      // ", "
      //           << curr_dir_y << std::endl;
    }
  }
};

// remove forces (after movement)
void MechEC_agent::remove_forces(Localsvector2d &locals,
                                 Agentvector2d &newstate,
                                 std::pair<int, int> pos) {
  // go through the v["f" + std::to_string(x_step) + std::to_string(y_step)]
  for(int x_step = -1; x_step < 2; x_step++) {
    for(int y_step = -1; y_step < 2; y_step++) {
      std::string Fcurr_pos =
          "f" + std::to_string(x_step) + std::to_string(y_step);
      if(v[Fcurr_pos] > 0) {
        // reset FTx, FTy, Eps accordingly
        if(pow(x_step, 2) - pow(y_step, 2) == 0) {
          locals.contents["FTx"]
              ->values[pos.first + x_step][pos.second + y_step] -=
              -1 * v[Fcurr_pos] * 1 / std::sqrt(2) * x_step;
          locals.contents["FTy"]
              ->values[pos.first + x_step][pos.second + y_step] -=
              -1 * v[Fcurr_pos] * 1 / std::sqrt(2) * y_step;
        } else {
          locals.contents["FTx"]
              ->values[pos.first + x_step][pos.second + y_step] -=
              -1 * v[Fcurr_pos] * x_step;
          locals.contents["FTy"]
              ->values[pos.first + x_step][pos.second + y_step] -=
              -1 * v[Fcurr_pos] * y_step;
        }
        // if(locals.contents["Eps"]
        //        ->values[pos.first + x_step][pos.second + y_step] < 0) {
        //   std::cout << "remove_forces," << id << ": Eps is already negative:"
        //             << locals.contents["Eps"]
        //                    ->values[pos.first + x_step][pos.second + y_step]
        //             << ", val is " << v[Fcurr_pos] << std::endl;
        // }
        locals.contents["Eps"]
            ->values[pos.first + x_step][pos.second + y_step] -=
            v[Fcurr_pos];  // v[Fcurr_pos] / std::sqrt(pow(x_step, 2) +
                           // pow(y_step, 2));
        // if(locals.contents["Eps"]
        //        ->values[pos.first + x_step][pos.second + y_step] < 0) {
        //   std::cout << "remove_forces," << id << ": Eps is now negative:"
        //             << locals.contents["Eps"]
        //                    ->values[pos.first + x_step][pos.second + y_step]
        //             << ", val is " << v[Fcurr_pos] << " @x,y=" << pos.first
        //             << "," << pos.second << " and step: " << x_step << ","
        //             << y_step << " and direction: " << v["xdir"] << ", "
        //             << v["ydir"] << std::endl;
        // }
        // and, of course, set Fcurr_pos to 0
        v[Fcurr_pos] = 0;
      }
    }
  }
};
