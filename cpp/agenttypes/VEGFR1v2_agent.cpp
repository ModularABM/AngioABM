#include "VEGFR1v2_agent.h"

// to throw exceptions
struct missing_variable {
  std::string varname;

  missing_variable() { varname = "NA"; };
  missing_variable(std::string varname_value) { varname = varname_value; };
};


// constructor
VEGFR1v2_agent::VEGFR1v2_agent(int x_val, int y_val, int id_val,
                               std::unordered_map<std::string, double> *v_val,
                               std::unordered_map<std::string, double> *p_val)
    : Agent(x_val, y_val, id_val, "VEGFR1v2_agent", v_val, p_val) {
  type = "VEGFR1v2_agent";

  // safety check: are all variables included?
  std::vector<std::string> v_names = {
      "dll4",        "dll4_mRNA",    "filopodia",   "mvegfr1",
      "nicd",        "notch",        "notch_mRNA",  "svegfr1",
      "vegfr1_mRNA", "vegfr2",       "vegfr2_mRNA", "vegfr2a",
      "tip",         "mvegfr1bound", "vegfr2bound", "tip_hist"};
  for(unsigned i = 0; i < v_names.size(); ++i) {

    if(v.find(v_names[i]) == v.end()) {

      throw missing_variable(v_names[i]);
    }
  }

  // safety check: are all parameters included?
  std::vector<std::string> p_names = {"dll4_tc_a",
                                      "dll4_tc_h",
                                      "dll4_tc_M0",
                                      "filo_k1",
                                      "filo_k2",
                                      "filo_k3",
                                      "filo_k4",
                                      "filo_k5",
                                      "k_mRNA_degradation",
                                      "k_protein_deg_dll4",
                                      "k_protein_deg_mvegfr1",
                                      "k_protein_deg_notch",
                                      "k_protein_deg_vegfr2",
                                      "k_protein_deg_vegfr2a",
                                      "k_protein_deg_svegfr1",
                                      "k_svegfr1_secretion",
                                      "k_vegfr1vegf_binding",
                                      "k_vegfrXvegf_diss",
                                      "k_vegfr2vegf_binding",
                                      "Vmax_translation",
                                      "k_vegfr2a_inact",
                                      "Vmax_translation",
                                      "kc",
                                      "kt",
                                      "k_nicd_degradation",
                                      "notch_tc_a",
                                      "notch_tc_h",
                                      "notch_tc_M0",
                                      "vegfr1_tc_a",
                                      "vegfr1_tc_h",
                                      "vegfr1_tc_M0",
                                      "vegfr2_tc_a",
                                      "vegfr2_tc_h",
                                      "vegfr2_tc_M0",
                                      "Vmax_tc_dll4",
                                      "Vmax_tc_notch",
                                      "Vmax_tc_vegfr1",
                                      "Vmax_tc_vegfr2",
                                      "Vmax_tl_dll4",
                                      "Vmax_tl_mvegfr1",
                                      "Vmax_tl_notch",
                                      "Vmax_tl_svegfr1",
                                      "Vmax_tl_vegfr2",
                                      "filo_threshold",
                                      "dll4_threshold",
                                      "uptake_factor",
                                      "filo_factor"};
  for(unsigned i = 0; i < p_names.size(); ++i) {

    if(p.find(p_names[i]) == p.end()) {

      throw missing_variable(p_names[i]);
    }
  }
};


// update internal states (and environment)
void VEGFR1v2_agent::update(Globalsvector &globals, Localsvector2d &locals,
                            Agentvector2d &oldstate, Agentvector2d &newstate,
                            double timepoint, int t_scale, int t_intstep,
                            std::mt19937 &rng) {


  // sense environment
  // double vegf_here = sense_nb_vegf(locals, x, y);
  int s_range = static_cast<int>(v["filopodia"] / p["filo_factor"] +
                                 1);  // actin represents lamellopodial length
  double vegf_here = sense_nb_localvar(locals, "vegf", x, y, s_range);
  double vegf_nb   = sense_nb_localvar(locals, "vegf", x, y, 1);
  double dll4_nbs  = sense_nb_variable(oldstate, "dll4");

  double vegfr2aa = v["vegfr2a"] + v["vegfr2bound"];

  // determine rate eqs:
  double v_deltanotch_cisInhibition = std::min(
      std::min((p["kc"] * v["dll4"] * v["notch"]) / t_scale, v["dll4"]),
      v["notch"]);
  double v_deltanotch_transActivation =
      std::min(std::min((p["kt"] * dll4_nbs * v["notch"]) / t_scale, dll4_nbs),
               v["notch"]);
  double v_dll4_degradation = (v["dll4"] * p["k_protein_deg_dll4"]) / t_scale;
  double v_dll4_translation = (p["Vmax_tl_dll4"] * v["dll4_mRNA"]) / t_scale;
  double v_dll4mRNA_degradation =
      (v["dll4_mRNA"] * p["k_mRNA_degradation"]) / t_scale;
  double v_dll4mRNA_transcription =
      (p["Vmax_tc_dll4"] *
       (1 / p["dll4_tc_a"] +
        (1 - 1 / p["dll4_tc_a"]) * pow(vegfr2aa, p["dll4_tc_h"]) /
            (pow(vegfr2aa, p["dll4_tc_h"]) +
             pow(p["dll4_tc_M0"], p["dll4_tc_h"])))) /
      t_scale;
  double v_filopodia_extension = (p["filo_k1"] * vegfr2aa) / t_scale;
  double v_filopodia_active_retraction =
      (p["filo_k2"] * v["filopodia"] *
       (1 -
        pow(vegfr2aa, p["filo_k3"]) /
            (pow(vegfr2aa, p["filo_k3"]) + pow(p["filo_k4"], p["filo_k3"])))) /
      t_scale;
  double v_filopodia_basal_retraction =
      (p["filo_k5"] * v["filopodia"]) / t_scale;
  double v_mvegfr1_degradation =
      (v["mvegfr1"] * p["k_protein_deg_mvegfr1"]) / t_scale;
  double v_mvegfr1_translation =
      (0.5 * p["Vmax_tl_mvegfr1"] * v["vegfr1_mRNA"]) / t_scale;
  double v_mvegfr1_vegfbinding = std::min(
      std::min((p["k_vegfr1vegf_binding"] * v["mvegfr1"] * vegf_nb) / t_scale,
               vegf_nb),
      v["mvegfr1"]);
  double v_mvegfr1_vegfdissoc =
      std::min((p["k_vegfrXvegf_diss"] * v["mvegfr1bound"]) / t_scale,
               v["mvegfr1bound"]);
  double v_nicd_degradation = (p["k_nicd_degradation"] * v["nicd"]) / t_scale;
  double v_notch_degradation =
      (v["notch"] * p["k_protein_deg_notch"]) / t_scale;
  double v_notch_translation = (p["Vmax_tl_notch"] * v["notch_mRNA"]) / t_scale;
  double v_notchmRNA_degradation =
      (v["notch_mRNA"] * p["k_mRNA_degradation"]) / t_scale;
  double v_notchmRNA_transcription =
      (p["Vmax_tc_notch"] *
       (1 / p["notch_tc_a"] +
        (1 - 1 / p["notch_tc_a"]) * pow(v["nicd"], p["notch_tc_h"]) /
            (pow(v["nicd"], p["notch_tc_h"]) +
             pow(p["notch_tc_M0"], p["notch_tc_h"])))) /
      t_scale;
  double v_svegfr1_secretion = std::min(
      (p["k_svegfr1_secretion"] * v["svegfr1"]) / t_scale, v["svegfr1"]);
  double v_svegfr1_degradation =
      (v["svegfr1"] * p["k_protein_deg_svegfr1"]) / t_scale;
  double v_svegfr1_translation =
      (0.5 * p["Vmax_tl_svegfr1"] * v["vegfr1_mRNA"]) / t_scale;
  double v_vegfr1mRNA_degradation =
      (v["vegfr1_mRNA"] * p["k_mRNA_degradation"]) / t_scale;
  double v_vegfr1mRNA_transcription =
      (p["Vmax_tc_vegfr1"] *
       (1 / p["vegfr1_tc_a"] +
        (1 - 1 / p["vegfr1_tc_a"]) * pow(v["nicd"], p["vegfr1_tc_h"]) /
            (pow(v["nicd"], p["vegfr1_tc_h"]) +
             pow(p["vegfr1_tc_M0"], p["vegfr1_tc_h"])))) /
      t_scale;
  double v_vegfr2_degradation =
      (v["vegfr2"] * p["k_protein_deg_vegfr2"]) / t_scale;
  double v_vegfr2_translation =
      (p["Vmax_tl_vegfr2"] * v["vegfr2_mRNA"]) / t_scale;
  double v_vegfr2_VEGFbinding = std::min(
      std::min((p["k_vegfr2vegf_binding"] * v["vegfr2"] * vegf_here) / t_scale,
               v["vegfr2"]),
      vegf_here);
  double v_vegfr2vegf_dissociation =
      (v["vegfr2bound"] * p["k_vegfrXvegf_diss"]) / t_scale;
  double v_vegfr2a_degradation =
      (v["vegfr2a"] * p["k_protein_deg_vegfr2a"]) / t_scale;
  double v_vegfr2a_inactivation =
      (v["vegfr2a"] * p["k_vegfr2a_inact"]) / t_scale;
  double v_vegfr2mRNA_degradation =
      (v["vegfr2_mRNA"] * p["k_mRNA_degradation"]) / t_scale;
  double v_vegfr2mRNA_transcription =
      (p["Vmax_tc_vegfr2"] *
       (1 -
        (1 - 1 / p["vegfr2_tc_a"]) * pow(v["nicd"], p["vegfr2_tc_h"]) /
            (pow(v["nicd"], p["vegfr2_tc_h"]) +
             pow(p["vegfr2_tc_M0"], p["vegfr2_tc_h"])))) /
      t_scale;

  // modify environment
  if(v_vegfr2_VEGFbinding > 0 or v_vegfr2vegf_dissociation > 0) {
    rem_nb_localvar(locals, "vegf", x, y,
                    v_vegfr2_VEGFbinding / p["uptake_factor"] -
                        v_vegfr2vegf_dissociation / p["uptake_factor"],
                    s_range);
  }

  if(v_mvegfr1_vegfbinding > 0 or v_mvegfr1_vegfdissoc > 0) {
    rem_nb_localvar(locals, "vegf", x, y,
                    v_mvegfr1_vegfbinding / p["uptake_factor"] -
                        v_mvegfr1_vegfdissoc / p["uptake_factor"],
                    s_range);
  }

  if(v_svegfr1_secretion > 0) {
    secrete_nb_localvar(locals, "svegfr1", x, y,
                        v_svegfr1_secretion / p["uptake_factor"]);
  }

  // update internal variables
  v["dll4"] = std::max(0.0,
                       v["dll4"] - v_deltanotch_cisInhibition +
                           v_dll4_translation - v_dll4_degradation);
  v["notch"] =
      std::max(0.0,
               v["notch"] - v_deltanotch_cisInhibition + v_notch_translation -
                   v_notch_degradation - v_deltanotch_transActivation);
  v["mvegfr1"] =
      std::max(0.0,
               v["mvegfr1"] + v_mvegfr1_translation - v_mvegfr1_degradation -
                   v_mvegfr1_vegfbinding + v_mvegfr1_vegfdissoc);
  v["vegfr2"] =
      std::max(0.0,
               v["vegfr2"] + v_vegfr2_translation - v_vegfr2_degradation -
                   v_vegfr2_VEGFbinding + v_vegfr2a_inactivation);
  v["svegfr1"] = std::max(0.0,
                          v["svegfr1"] + v_svegfr1_translation -
                              v_svegfr1_secretion - v_svegfr1_degradation);
  v["dll4_mRNA"] = std::max(
      0.0, v["dll4_mRNA"] + v_dll4mRNA_transcription - v_dll4mRNA_degradation);
  v["notch_mRNA"] = std::max(0.0,
                             v["notch_mRNA"] + v_notchmRNA_transcription -
                                 v_notchmRNA_degradation);
  v["vegfr1_mRNA"] = std::max(0.0,
                              v["vegfr1_mRNA"] + v_vegfr1mRNA_transcription -
                                  v_vegfr1mRNA_degradation);
  v["vegfr2_mRNA"] = std::max(0.0,
                              v["vegfr2_mRNA"] + v_vegfr2mRNA_transcription -
                                  v_vegfr2mRNA_degradation);
  v["filopodia"] = std::max(0.0,
                            v["filopodia"] + v_filopodia_extension -
                                v_filopodia_active_retraction -
                                v_filopodia_basal_retraction);
  v["nicd"] = std::max(
      0.0, v["nicd"] + v_deltanotch_transActivation - v_nicd_degradation);
  v["vegfr2a"] = std::max(0.0,
                          v["vegfr2a"] + v_vegfr2vegf_dissociation -
                              v_vegfr2a_degradation - v_vegfr2a_inactivation);
  v["mvegfr1bound"] = std::max(
      0.0, v["mvegfr1bound"] + v_mvegfr1_vegfbinding - v_mvegfr1_vegfdissoc);
  v["vegfr2bound"] = std::max(
      0.0, v["vegfr2bound"] + v_vegfr2_VEGFbinding - v_vegfr2vegf_dissociation);


  // remove from neighbours
  rem_nb_variable(newstate, "dll4", v_deltanotch_transActivation);

  // determine tip
  v["tip"] = v["vegfr2_mRNA"] > v["vegfr1_mRNA"] and
                     v["filopodia"] > p["filo_threshold"] and
                     v["dll4_mRNA"] > p["dll4_threshold"]
                 ? 1
                 : 0;
  if(v["tip"] == 1) {
    v["tip_hist"] = 1;
  }

  // move towards gradient if tip
  if(v["tip"] == 1 && t_intstep == 0) {


    // read out gradient (find patch with maximum vegf value in s_range)
    int s_range =  // v["filopodia"] +
        1;

    std::pair<int, int> to_patch =
        turn_up_gradient(locals, "vegf", s_range, rng);

    std::pair<int, int> oldpos = {x, y};
    std::vector<int> oldneighbours = neighbours;

    // check for non-tip neighbours that could proliferate and fill the empty
    // space
    std::vector<int> nontip_EC_neighbours;
    for(auto it = oldneighbours.begin(); it != oldneighbours.end(); ++it) {

      Agent *curr_agent = newstate.agents[*it];
      if(VEGFR1v2_agent *curr_magent =
             dynamic_cast<VEGFR1v2_agent *>(curr_agent)) {

        if(curr_magent->v["tip"] == 0.0) {

          nontip_EC_neighbours.push_back(*it);
        }
      }
    }
    int did_move = 1;

    // can only move when there is a stalk cell to fill the emptyt spot
    if(nontip_EC_neighbours.size() > 0) {

      // use a little wiggling here already
      int x_step = to_patch.first;
      int y_step = to_patch.second;

      // try wiggling left and right randomly
      std::pair<int, int> wiggle_aclock = turn(x_step, y_step, 0);
      std::pair<int, int> wiggle_clock  = turn(x_step, y_step, 1);

      std::vector<std::pair<int, int>> new_dirs = {
          wiggle_aclock, wiggle_clock, to_patch, to_patch, to_patch};
      shuffle(new_dirs.begin(), new_dirs.end(), rng);

      did_move = newstate.move_agent(id, new_dirs[0].first, new_dirs[0].second);
      new_dirs.erase(new_dirs.begin());
      while(did_move == 1 and new_dirs.size() > 0) {
        did_move =
            newstate.move_agent(id, new_dirs[0].first, new_dirs[0].second);
        new_dirs.erase(new_dirs.begin());
      }

      // try wiggling randomly
      int counter = 0;
      while((did_move == 1) & (counter < 10)) {
        std::pair<int, int> rand_dir = wiggle();
        did_move = newstate.move_agent(id, rand_dir.first, rand_dir.second);
        counter++;
      }

      // insert new stalk agent if movement was succesful
      if(did_move == 0) {

        long unsigned random_nb_idx = rng() % (int)nontip_EC_neighbours.size();
        int random_nb               = nontip_EC_neighbours[random_nb_idx];
        VEGFR1v2_agent *curr_agent =
            dynamic_cast<VEGFR1v2_agent *>(newstate.agents[random_nb]);
        VEGFR1v2_agent new_stalkcell = *curr_agent;

        newstate.add_agent_noisykid(oldpos.first, oldpos.second, new_stalkcell,
                                    rng);
      }
    }
  }
};

void VEGFR1v2_agent::initialize(Globalsvector &globals, Localsvector2d &locals,
                                Agentvector2d &agents, double t, int t_scale) {
  // throw error message if locals has no vegf/svegfr1
  if(locals.contents.count("vegf") < 1) {
    throw missing_variable("vegf");
  }
  if(locals.contents.count("svegfr1") < 1) {
    throw missing_variable("svegfr1");
  }
};
