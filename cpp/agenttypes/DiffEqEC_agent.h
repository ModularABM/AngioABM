////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef DIFFEQECAGENT_H
#define DIFFEQECAGENT_H

#include <algorithm>    // using, at least, std::max_element
#include <random>
#include <ctime>
#include <unordered_map>
#include "sys/time.h"

#include "../core/Agent.h"

// class of EC-Agents that are based on difference_equations
// model based on MOSAIC model by Carlier et al, but modified
// to have more realistic intracellular states and added VEGFR1

class DiffEqEC_agent : public Agent {
public:
  // ___________________________________________________________________________
  // constructors:
  DiffEqEC_agent(int x_val, int y_val, int id_val,
                 std::unordered_map<std::string, double> *v_val,
                 std::unordered_map<std::string, double> *p_val);


  // ___________________________________________________________________________
  // member functions:


  void update(Globalsvector &globalvars, Localsvector2d &localvars,
              Agentvector2d &oldstate, Agentvector2d &newstate, double t,
              int t_scale, int t_intstep, std::mt19937 &rng);

  void initialize(Globalsvector &globals, Localsvector2d &locals,
                          Agentvector2d &agents, double t, int t_scale);
};

#endif
