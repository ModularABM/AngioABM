#include "NaivediffusionWS_lv.h"

// Implementation of NaivediffusionWS_lv class


// constructor
NaivediffusionWS_lv::NaivediffusionWS_lv(
    std::string initname, std::vector<std::vector<double>> init_values,
    double diffusion_rate_val, double degradation_rate_val,
    std::vector<std::vector<double>> source_val)
    : Localvar(initname, init_values) {
  diffusion_rate   = diffusion_rate_val;
  degradation_rate = degradation_rate_val;
  source           = source_val;
};


// diffuse and degrade
void NaivediffusionWS_lv::update(Globalsvector &globals, Localsvector2d &locals,
                                 Agentvector2d &agents, double t, int t_scale) {

  int idim = static_cast<int>(values.size());
  int jdim = static_cast<int>(values[0].size());

  diffuse_naive(diffusion_rate / t_scale, idim, jdim, agents);

  degrade(degradation_rate / t_scale, idim, jdim);

  // add to source
  increase_coords(source, t_scale);
};
