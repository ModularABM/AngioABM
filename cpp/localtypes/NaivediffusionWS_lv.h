////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef NAIVEDIFFUSIONWS_LV_H
#define NAIVEDIFFUSIONWS_LV_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include "../core/Localvar.h"

// Local variable class that diffuses and degrades with rectangular source
// allows for anisotropic diffusion but needs mapping of D to diffusion_rate
class NaivediffusionWS_lv : public Localvar {
public:
  double diffusion_rate, degradation_rate;
  std::vector<std::vector<double>> source;


  // ___________________________________________________________________________
  // constructors:
  NaivediffusionWS_lv(std::string initname,
                      std::vector<std::vector<double>> init_values,
                      double diffusion_rate_val   = 0,
                      double degradation_rate_val = 0,
                      std::vector<std::vector<double>> source_coords_val = {{0, 0}});


  // ___________________________________________________________________________
  // member functions:
  virtual void update(Globalsvector &globals, Localsvector2d &locals,
                      Agentvector2d &agents, double t,
                      int t_scale);  // diffuse and degrade
};

#endif
