#include "svegfr1_lv.h"

// Implementation of svegfr1_lv

// constructor:
svegfr1_lv::svegfr1_lv(std::string initname,
                       std::vector<std::vector<double>> init_values,
                       double diff_D_val, double deg_rate_val,
                       std::vector<std::vector<double>> source_val,
                       double binding_const_val, double grid_size_val,
                       double step_size_val)
    : Localvar(initname, init_values) {

  diff_D    = diff_D_val;
  deg_rate  = deg_rate_val;
  source    = source_val;
  grid_size = grid_size_val;
  step_size = step_size_val;
  bind_k    = binding_const_val;
};


// update: like FinDiffDiffusion_lv, but with eating up vegfr
void svegfr1_lv::update(Globalsvector &globals, Localsvector2d &locals,
                        Agentvector2d &agents, double t, int t_scale) {

  int idim = static_cast<int>(values.size());
  int jdim = static_cast<int>(values[0].size());

  // D*deltaT/deltaX^2 - in square grid can compute once and use for all
  double diff_factor = diff_D * step_size / pow(grid_size, 2) / t_scale;

  // decrease step size until diff_factor is smaller than 0.5 (for stability)
  int add_steps = 1;
  while(diff_factor > 0.1) {
    add_steps *= 2;
    diff_factor =
        diff_D * step_size / (add_steps * t_scale) / pow(grid_size, 2);
  }


  // iterate over additional stepsize
  for(int k = 0; k < add_steps; k++) {

    // make a copy of self to use for updating
    std::vector<std::vector<double>> old_val(values);
    std::vector<std::vector<double>> new_val(idim,
                                             (std::vector<double>(jdim, 0.0)));

    for(int i = 0; i < idim; i++) {

      for(int j = 0; j < jdim; j++) {

        double d2u_dx2;
        // boundary condition x1
        if(i == 0) {
          d2u_dx2 = diff_factor *
                    (old_val[i][j] - 2 * old_val[i][j] + old_val[i + 1][j]);
        }
        // boundary condition x2
        else if(i == idim - 1) {
          d2u_dx2 = diff_factor *
                    (old_val[i - 1][j] - 2 * old_val[i][j] + old_val[i][j]);
        }
        // normal fields
        else {
          d2u_dx2 = diff_factor *
                    (old_val[i - 1][j] - 2 * old_val[i][j] + old_val[i + 1][j]);
        }

        double d2u_dy2;
        // boundary condition y1
        if(j == 0) {
          d2u_dy2 = diff_factor *
                    (old_val[i][j] - 2 * old_val[i][j] + old_val[i][j + 1]);
        }
        // boundary condition y2
        else if(j == jdim - 1) {
          d2u_dy2 = diff_factor *
                    (old_val[i][j - 1] - 2 * old_val[i][j] + old_val[i][j]);
        }
        // normal fields
        else {
          d2u_dy2 = diff_factor *
                    (old_val[i][j - 1] - 2 * old_val[i][j] + old_val[i][j + 1]);
        }

        // put diffusion together
        new_val[i][j] = old_val[i][j] + d2u_dx2 + d2u_dy2;


        // degradation
        new_val[i][j] *= (1 - deg_rate / (add_steps * t_scale));

        // insert small fuckup to prevent BIG fuckups
        if(std::isinf(new_val[i][j])) {
          new_val[i][j] = 0;
        }
      }
    }

    values = new_val;

    // bind vegfr and unbind
    for(int i = 0; i < idim; i++) {

      for(int j = 0; j < jdim; j++) {

        double binding_rate =
            std::min(std::min(values[i][j] * bind_k / (add_steps * t_scale) *
                                  locals.contents["vegf"]->values[i][j],
                              values[i][j]),
                     locals.contents["vegf"]->values[i][j]);

        values[i][j] -= binding_rate;
        locals.contents["vegf"]->values[i][j] -= binding_rate;
        locals.contents["svegfr1bound"]->values[i][j] += binding_rate;
      }
    }
  }
};
