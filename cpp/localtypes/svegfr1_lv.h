////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef SVEGFR1_LV_H
#define SVEGFR1_LV_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include "../core/Localvar.h"

// Local variable class that diffuses and degrades like FinDiffDiffusion_lv
// Additionally eats up vegfr
// Uses finite differences to compute diffusion, grid is assumed to be square.
// Simple check and correction for stability between D and t is applied.
class svegfr1_lv : public Localvar {
public:
  double diff_D, deg_rate;
  double bind_k;
  double grid_size, step_size;
  std::vector<std::vector<double>> source;

  // ___________________________________________________________________________
  // constructor:
  svegfr1_lv(std::string initname, std::vector<std::vector<double>> init_values,
             double diff_D_val = 0, double deg_rate_val = 0,
             std::vector<std::vector<double>> source_val = {{0}},
             double binding_const_val = 0,
             double grid_size_val = 1.0, double step_size_val = 1.0);


  // ___________________________________________________________________________
  // member functions:
  virtual void update(Globalsvector &globals, Localsvector2d &locals,
                      Agentvector2d &agents, double t, int t_scale);  // diffuse and degrade
};

#endif
