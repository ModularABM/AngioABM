#include "ADEDiffDiffusion_lv.h"

// Implementation of ADEDiffDiffusion_lv class


// constructor:
ADEDiffDiffusion_lv::ADEDiffDiffusion_lv(
    std::string initname, std::vector<std::vector<double>> init_values,
    double diff_D_val, double deg_rate_val,
    std::vector<std::vector<double>> source_val, double grid_size_val,
    double step_size_val, double bv_val)
    : Localvar(initname, init_values) {

  diff_D    = diff_D_val;
  deg_rate  = deg_rate_val;
  source    = source_val;
  grid_size = grid_size_val;
  step_size = step_size_val;
  bv        = bv_val;

  assert(source.size() == values.size());
};


// diffuse and degrade
void ADEDiffDiffusion_lv::update(Globalsvector &globals, Localsvector2d &locals,
                                 Agentvector2d &agents, double t, int t_scale) {

  int idim = static_cast<int>(values.size());
  int jdim = static_cast<int>(values[0].size());

  // D*deltaT/deltaX^2 - in square grid can compute once and use for all
  double diff_factor = (diff_D * step_size / pow(grid_size, 2)) / t_scale;

  // decrease step size until diff_factor is smaller than 0.5 (for stability)
  int add_steps = 1;
  while(diff_factor > 0.1) {
    add_steps *= 2;
    diff_factor =
        diff_D * step_size / pow(grid_size, 2) / (add_steps * t_scale);
  }

  // iterate over additional stepsize
  for(int k = 0; k < add_steps; k++) {

    // using generic functions
    diffuse_ade(diff_factor, step_size, grid_size, idim, jdim, bv);

    degrade(deg_rate / (add_steps * t_scale), idim, jdim);

    increase_coords(source, add_steps * t_scale);
  }
};
