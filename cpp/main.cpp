////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////


#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include <random>
#include <iterator>
#include <ctime>
#include <vector>
#include "core/Agent.h"
#include "core/Agentvector.h"
#include "core/Localvar.h"
#include "core/Globalsvector.h"
#include "core/ABModel.h"
#include "localtypes/NaivediffusionWS_lv.h"
#include "localtypes/FinDiffDiffusion_lv.h"
#include "localtypes/ADEDiffDiffusion_lv.h"
#include "agenttypes/DiffEqEC_agent.h"
#include "core/ReadXMLcues.h"
#include "boost/program_options.hpp"
#include "agenttypes/VEGFR1_agent.h"
#include "agenttypes/VEGFR1v2_agent.h"
#include "agenttypes/MechEC_agent.h"

// main for running simulations of agent based models specified as xml files.
// call like this:
/*
Angio_test1_ABM.exe example.xml test.csv
//name                model_def   outputname
*/

namespace {
const size_t ERROR_IN_COMMAND_LINE     = 1;
const size_t SUCCESS                   = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

}  // namespace


// randomness:
std::mt19937 rng;


// structs for throwing expressions
struct missing_variable {
  std::string varname;
  missing_variable() { varname = "NA"; };
  missing_variable(std::string varname_value) { varname = varname_value; };
};


// main loop
int main(int argc, char **argv) {

  time_t start = time(0);

  try {
    /** Define and parse the program options
     */
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()("help", "Print help messages")(
        "xml,X", po::value<std::string>()->required(), "model file")(
        "verbose", po::value<int>()->default_value(0), "change verbosity");

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, desc), vm);  // can throw

      /** --help option
       */
      if(vm.count("help")) {
        std::cout << "Basic Command Line Parameter App" << std::endl
                  << desc << std::endl;
        return SUCCESS;
      }

      po::notify(vm);  // throws on error, so do after help in case
                       // there are any problems
    } catch(po::error &e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
    }

    std::string xmlfilename = vm["xml"].as<std::string>();
    int verbose             = vm["verbose"].as<int>();

    rng.seed(static_cast<unsigned int>(time(0)));

    // create a model from xml and simulate
    ABModel2d le_model(0, 0);
    try {
      le_model = constructModel(xmlfilename, verbose, rng);

    } catch(missing_variable &stop) {
      std::cout << "EXCEPTION: missing variable:" << stop.varname << "\n";
      return 1;
    }

    // just output the names of the localvars in le_model
    // std::cout << "Model contains localvariables:\n";
    // for(const auto &myPair : le_model.locals.contents) {
    //   std::cout << "\t" << myPair.first << "\n";
    // }


    // output
    le_model.cleanse_output();
    le_model.output_dt();

    // simulate:
    while(le_model.t < le_model.t_max) {

      // progress indicator
      if(verbose > 0) {
        if(le_model.t % 20 == 0) {
          std::cout << le_model.t;
        } else {
          // std::cout << "." << le_model.t << ".";
          std::cout << ".";
        }
      }

      try {
        le_model.update(0, 1, rng);  // 0: async, 1: randorder
      } catch(missing_variable &stop) {
        std::cout << "EXCEPTION: missing variable:" << stop.varname << "\n";
        return 1;
      } catch(out_of_bounds &stop) {
        std::cout << "EXCEPTION:out of bounds:" << stop.varname << "\n";
        return 1;
      }
      le_model.output_dt();
    }
    if(verbose > 0) {
      std::cout << "sim done"
                << "\n";
    }

    // print runtime
    double seconds_since_start = difftime(time(0), start);
    if(verbose > 0) {
      std::cout << "Runtime was " << seconds_since_start << "\n";
    }

  } catch(std::exception &e) {
    std::cerr << "Unhandled Exception reached the top of main: " << e.what()
              << ", application will now exit" << std::endl;
    return ERROR_UNHANDLED_EXCEPTION;
  }

  return SUCCESS;
}
