////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef GLOBALSVECTOR_H
#define GLOBALSVECTOR_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include "Localsvector.h"

// forward declarations to resolve circ. dep.
class Localsvector2d;
class Agentvector2d;


// Container class for global variables (name, value pairs)
class Globalsvector {
public:
  std::map<std::string, double> contents;

  // ___________________________________________________________________________
  // Constructors:

  Globalsvector();
  Globalsvector(std::vector<std::string> init_names,
                std::vector<double> init_values);
  Globalsvector(std::vector<std::pair<std::string, double>> inits);
  Globalsvector(std::map<std::string, double> inits);


  // ___________________________________________________________________________
  // member functions:

  // update global variables
  void update(Globalsvector &globals, Localsvector2d &locals,
              Agentvector2d &agents, double t, int t_scale);

  // R data.frame
  void output_dt(std::ostream &target = std::cout, double t = 0);

  // initialize
  void initialize(Globalsvector &globals, Localsvector2d &locals,
                  Agentvector2d &agents, double t, int t_scale);
};

#endif
