////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////


#ifndef ABMODEL_H
#define ABMODEL_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>  
#include <vector>
#include "Agent.h"
#include "Agentvector.h"
#include "Localvar.h"
#include "Globalsvector.h"


// class ABModel (as in Agent Based Model)
// combines Agentvector, Localsvector, Globalsvector and provides basic
// functions
// Also contains information on how to output

class ABModel2d {
public:
  std::pair<int, int> maxcoords;
  int t;                   // current time point
  int t_max;               // last time point for simulation
  int t_scale;             // scaling factor to use smaller step size
  int out_type;            // 0 -> timepoints, 1 -> sequence
  std::vector<int> out_t;  // time points at which to output
  std::string out_name;
  std::string out_format;
  Globalsvector globals;
  Localsvector2d locals;
  Agentvector2d agents;


  // constructors:
  ABModel2d(int xmax, int ymax);
  ABModel2d(int xmax, int ymax, Globalsvector initglobals,
            Localsvector2d initlocals, Agentvector2d initagents, int t_max,
            int t_scale, int output_type_val, std::vector<int> out_t_val,
            std::string out_name_val, std::string output_format_val);
  ABModel2d(int xmax, int ymax, std::vector<std::string> initglobalnames,
            std::vector<std::string> initlocalnames, Agentvector2d initagents,
            int t_max, int t_scale);

  // member functions:
  void update(int sync, int randorder,
              std::mt19937 &rng);  // 1 step, sync/async, random/not
  void output_dt();                // wrapper to check outputdef before output
  void output_dt_raw();            // output for importing into R data.frame
  void output_dt_compact();            // output for importing into R data.frame
  void cleanse_output();           // delete old output files
  void cleanse_output_compact();           // delete old output files
  void initialize();                 // check if all necessary agents/locals have been constructed
};

#endif
