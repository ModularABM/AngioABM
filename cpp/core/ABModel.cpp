#include "ABModel.h"


// class ABModel (as in Agent Based Model)
// combines Agentvector, Localsvector, Globalsvector and provides basic
// functions


// create empty
ABModel2d::ABModel2d(int xmax, int ymax) {

  maxcoords.first                   = xmax;
  maxcoords.second                  = ymax;
  t                                 = 0;
  t_max                             = 0;
  t_scale                           = 0;
  globals                           = Globalsvector();
  std::vector<std::string> emptyvec = std::vector<std::string>();
  locals                            = Localsvector2d(emptyvec, xmax, ymax);
  agents                            = Agentvector2d();
  out_type                          = 1;
  out_t                             = std::vector<int>{0, 0, 1};
  out_name                          = "default_out_";
  out_format                        = "compact";
};


// create initialized
ABModel2d::ABModel2d(int xmax, int ymax, Globalsvector initglobals,
                     Localsvector2d initlocals, Agentvector2d initagents,
                     int t_max_val, int t_scale_val, int out_type_val,
                     std::vector<int> out_t_val, std::string out_name_val,
                     std::string out_format_val) {

  maxcoords.first  = xmax;
  maxcoords.second = ymax;
  globals          = initglobals;
  locals           = initlocals;
  agents           = initagents;
  t                = 0;
  t_max            = t_max_val;
  t_scale          = t_scale_val;
  out_type         = out_type_val;
  out_t            = out_t_val;
  out_name         = out_name_val;
  out_format       = out_format_val;
};


// create with names and agents
ABModel2d::ABModel2d(int xmax, int ymax,
                     std::vector<std::string> initglobalnames,
                     std::vector<std::string> initlocalnames,
                     Agentvector2d initagents, int t_max_val, int t_scale_val) {

  maxcoords.first  = xmax;
  maxcoords.second = ymax;
  std::vector<double> globalzeroes(initglobalnames.size(), 0);
  globals    = Globalsvector(initglobalnames, globalzeroes);
  locals     = Localsvector2d(initlocalnames, xmax, ymax);
  agents     = initagents;
  t          = 0;
  t_max      = t_max_val;
  t_scale    = t_scale_val;
  out_type   = 1;  //
  out_t      = std::vector<int>{0, t_max, 1};
  out_name   = "default_out_";
  out_format = "compact";
};


// 1 step, sync/async, random/not as parameters
void ABModel2d::update(int sync, int randorder, std::mt19937 &rng) {

  for(int i = 0; i < t_scale; i++) {
    globals.update(globals, locals, agents, t, t_scale);
    locals.update(globals, locals, agents, t, t_scale);
    agents.update(globals, locals, agents, t, t_scale, i, sync, randorder, rng);
  }
  t++;
};


// wrapper to check outputdefinitions before calling output_datatable_raw
void ABModel2d::output_dt() {

  if(out_type == 0) {
    if(out_t[0] == t) {
      if(out_format == "full") {
        output_dt_raw();
      } else if(out_format == "compact") {
        output_dt_compact();
      } else if(out_format == "MultiCellDS") {
        // nothing here yet
      }
      out_t.erase(out_t.begin());
    }
  }

  else {
    if(((t + out_t[0]) % out_t[2]) == 0 && t < out_t[1] && t > out_t[0]) {
      if(out_format == "full") {
        output_dt_raw();
      } else if(out_format == "compact") {
        output_dt_compact();
      } else if(out_format == "MultiCellDS") {
        // nothing here yet
      }
    }
  }
};


// output for importing into R data.frame (x,y,t,varname, varvalue)
// takes up much space
void ABModel2d::output_dt_raw() {

  // open targets, append, and close again
  std::ofstream target;
  std::string targetname = out_name + ".globals.csv";
  target.open(targetname.c_str(), std::ios::app);
  globals.output_dt(target, t);
  target.close();

  targetname = out_name + ".locals.csv";
  target.open(targetname.c_str(), std::ios::app);
  locals.output_dt(target, t);
  target.close();

  targetname = out_name + ".agents.csv";
  target.open(targetname.c_str(), std::ios::app);
  agents.output_dt(target, t);
  target.close();
};


// output for importing into R data.frame (x,y,t,var1, var2, ....)
void ABModel2d::output_dt_compact() {

  // open targets, append, and close again
  std::ofstream target;
  std::string targetname = out_name + ".globals.csv";
  target.open(targetname.c_str(), std::ios::app);
  globals.output_dt(target, t);
  target.close();

  targetname = out_name + ".locals.csv";
  target.open(targetname.c_str(), std::ios::app);
  locals.output_compact(target, t);
  target.close();

  targetname = out_name + ".agents.csv";
  target.open(targetname.c_str(), std::ios::app);
  agents.output_compact(target, t);
  target.close();
};


// output_datatable opens for appending, so ensure file contains just the header
void ABModel2d::cleanse_output() {

  std::ofstream target;

  // globals
  std::string targetname = out_name + ".globals.csv";
  target.open(targetname.c_str(), std::ios::trunc);
  if(out_format == "full") {
    target << "x,y,t,type,name,value\n";
  } else if(out_format == "compact") {
    target << "t";
    for(auto iterator = globals.contents.begin();
        iterator != globals.contents.end(); iterator++) {

      target << "," << iterator->second;
    }
    target << "\n";
  }
  target.close();

  // locals
  targetname = out_name + ".locals.csv";
  target.open(targetname.c_str(), std::ios::trunc);
  if(out_format == "compact") {
    std::string names = "x,y,t";
    for(auto it = locals.contents.begin(); it != locals.contents.end(); it++) {
      names += "," + it->first;
    }
    names += "\n";
    target << names;
  } else {
    target << "x,y,t,type,name,value\n";
  }
  target.close();

  // agents
  targetname = out_name + ".agents.csv";
  target.open(targetname.c_str(), std::ios::trunc);
  if(out_format == "compact") {

    std::string currnames = "t,x,y,id,type,num_nbs";
    for(auto it = agents.headers.begin(); it != agents.headers.end(); it++) {
      currnames += "," + *it;
    }
    currnames += "\n";

    // std::vector<std::string> names;
    // std::vector<std::string> types;
    // std::unordered_map<std::string, std::string> agent_v;
    // // for all types check if we already got their names
    // for(auto it = agents.agents.begin(); it != agents.agents.end(); it++) {

    //   std::string thistype = it->second->type;
    //   if(agent_v.find(thistype) == agent_v.end()) {

    //     std::string currnames = "x,y,t,type,id";
    //     for(auto jt = it->second->v.begin(); jt != it->second->v.end(); jt++)
    //     {
    //       currnames += "," + jt->first;
    //     }
    //     currnames += "\n";
    //     names.push_back(currnames);
    //     types.push_back(thistype);
    //     agent_v[thistype] = currnames;
    //   }
    // }
    // if(agent_v.size() > 1) {
    //   target << "headers for agent types:\n";
    //   // insert headers here
    //   for(auto jt = agent_v.begin(); jt != agent_v.end(); jt++) {
    //     target << jt->second << "\n";
    //   }
    //   target << "\nx,y,t,type,id,values...\n";
    // } else {
    //   target << agent_v.begin()->second << "\n";
    // }
    target << currnames;

  } else {
    target << "x,y,t,type,name,value\n";
  }
  target.close();
};


// output_datatable opens for appending, so ensure file contains te correct
// header
void ABModel2d::cleanse_output_compact() {

  std::ofstream target;
  std::string targetname = out_name + ".globals.csv";
  target.open(targetname.c_str(), std::ios::trunc);
  target << "x,y,t,type,name,value\n";
  target.close();

  targetname = out_name + ".locals.csv";
  target.open(targetname.c_str(), std::ios::trunc);
  // here: get list of local variables and write them in the header
  target << "x,y,t";
  for(auto i = locals.contents.begin(); i != locals.contents.end(); ++i) {
    target << "," << i->first;
  }
  target << "\n";
  target.close();

  targetname = out_name + ".agents.csv";
  target.open(targetname.c_str(), std::ios::trunc);
  target << "x,y,t";
  // here: get one agent and write its variable names into the header
  // problem: if there are multiple agents, this will not be consistent
  for(auto it = agents.agents[0]->v.begin(); it != agents.agents[0]->v.end();
      ++it) {
    target << "," << it->first;
  }
  target << "\n";
  target.close();
};


// run initialization checks on all members
void ABModel2d::initialize() {

  for(int i = 0; i < t_scale; i++) {
    globals.initialize(globals, locals, agents, t, t_scale);
    locals.initialize(globals, locals, agents, t, t_scale);
    agents.initialize(globals, locals, agents, t, t_scale);
  }
  t++;
};
