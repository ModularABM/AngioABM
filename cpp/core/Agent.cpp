#include "Agent.h"

// Implementation of Agent class, parent for all other agents


// to throw exceptions
struct missing_variable {
  std::string varname;

  missing_variable() { varname = "NA"; };
  missing_variable(std::string varname_value) { varname = varname_value; };
};


// basic constructor
Agent::Agent(int x_val, int y_val, int id_val) {

  x          = x_val;
  y          = y_val;
  id         = id_val;
  neighbours = std::vector<int>();
  type       = "baseAgent";
  v          = std::unordered_map<std::string, double>();
  p          = std::unordered_map<std::string, double>();
};


// constructor with explicit type
Agent::Agent(int x_val, int y_val, int id_val, std::string type_val) {

  x          = x_val;
  y          = y_val;
  id         = id_val;
  neighbours = std::vector<int>();
  type       = type_val;
  v          = std::unordered_map<std::string, double>();
  p          = std::unordered_map<std::string, double>();
};


// constructor with explicit type and variables and parameters
Agent::Agent(int x_val, int y_val, int id_val, std::string type_val,
             std::unordered_map<std::string, double> *var_vals,
             std::unordered_map<std::string, double> *par_vals) {

  x          = x_val;
  y          = y_val;
  id         = id_val;
  neighbours = std::vector<int>();
  type       = type_val;
  v          = std::unordered_map<std::string, double>(*var_vals);
  p          = std::unordered_map<std::string, double>(*par_vals);
};


// move xstep and ystep from current position
// Agent has no knowledge of surroundings
// the respective checks have to be done from Agentvector2d class
void Agent::move(int x_step, int y_step) {

  x += x_step;
  y += y_step;
}


// set new position to newx, newy
// Agent has no knowledge of surroundings
// the respective checks have to be done from Agentvector2d class
void Agent::setpos(int x_new, int y_new) {

  x = x_new;
  y = y_new;
}


// set id, mostly for inserting new agents
void Agent::setid(int id_val) { id = id_val; }


// output state for reading on screen
void Agent::output_state(std::ostream &target) {

  target << "Agent" << id << " is at x=" << x << " and ypos=" << y;
  target << " and has neighbours: ";
  for(unsigned i = 0; i < neighbours.size(); i++) {
    target << neighbours[i] << ", ";
  }
  target << "\n";
}


// output for importing into R data.frame (x,y,t,type,varname, varvalue)
void Agent::output_dt(std::ostream &target, double t) {

  std::ostringstream s;
  s << x << "," << y << "," << t << ",agent,agentid," << id << "\n";
  s << x << "," << y << "," << t << ",agent,agenttype," << type << "\n";
  s << x << "," << y << "," << t << ",agent,neighbours," << neighbours.size()
    << "\n";
  for(auto iterator = v.begin(); iterator != v.end(); iterator++) {

    s << x << "," << y << "," << t << ",agent," << iterator->first << ","
      << iterator->second << "\n";
  }
  target << s.str();
}

// output for importing into R data.frame (x,y,t,type,varname, varvalue)
void Agent::output_compact(std::ostream &target, double t,
                           std::vector<std::string> headers) {

  std::ostringstream s;
  s << t << "," << x << "," << y << "," << id << "," << type << ","
    << neighbours.size();
  for(auto iterator = headers.begin(); iterator != headers.end(); iterator++) {

    if(v.count(*iterator) > 0) {
      s << "," << v[*iterator];  // .second;
    } else {
      s << ",NA";
    }
  }
  s << "\n";
  target << s.str();
}


// empty update-template
void Agent::update(Globalsvector &globals, Localsvector2d &locals,
                   Agentvector2d &oldstate, Agentvector2d &newstate, double t,
                   int t_scale, int t_intstep, std::mt19937 &rng){};

// empty initialize-template
void Agent::initialize(Globalsvector &globals, Localsvector2d &locals,
                       Agentvector2d &agents, double t, int t_scale){};

// get value of a specific localvariable in the neighbourhood.
double Agent::sense_nb_localvar(Localsvector2d locals, std::string l_name,
                                int x_pos, int y_pos, int s_range) {

  if(locals.contents.count(l_name) < 1) {
    throw missing_variable(l_name);
  }

  // std::cout << "sense_nb_localvar:" << std::endl;
  double ret_val = 0.0;
  std::vector<std::vector<double>> localmatrix =
      locals.contents[l_name]->values;
  for(int i = std::max(0, x_pos - s_range);
      i < std::min(x_pos + s_range + 1,
                   (int)(locals.contents[l_name]->values.size()));
      i++) {
    // std::cout << "\t" << i << std::endl;

    for(int j = std::max(0, y_pos - s_range);
        j < std::min(y_pos + s_range + 1,
                     (int)(locals.contents[l_name]->values[0].size()));
        j++) {
      // std::cout << "\t\t" << j << std::endl;

      ret_val += localmatrix[i][j];
      // std::cout << "localmatrix i,j=" << localmatrix[i][j]
      //           << ", retval=" << ret_val << std::endl;
    }
  }
  return ret_val;
};


// subtract from localvariable in neighbourhood.
void Agent::rem_nb_localvar(Localsvector2d &locals, std::string l_name,
                            int x_pos, int y_pos, double rem_val,
                            int rem_range) {

  if(locals.contents.count(l_name) < 1) {
    throw missing_variable(l_name);
  }

  int x_max = std::min(x_pos + rem_range + 1,
                       (int)(locals.contents[l_name]->values.size()));
  int x_min = std::max(0, x_pos - rem_range);
  int y_max = std::min(y_pos + rem_range + 1,
                       (int)(locals.contents[l_name]->values[0].size()));
  int y_min   = std::max(0, y_pos - rem_range);
  int num_nbs = (x_max - x_min + 1) * (y_max - y_min + 1);
  for(int i = x_min; i < x_max; i++) {

    for(int j = y_min; j < y_max; j++) {
      locals.contents[l_name]->values[i][j] = std::max(
          0.0, locals.contents[l_name]->values[i][j] - (rem_val / num_nbs));
    }
  }
};


// add to a localvariable in the neighbourhood,
void Agent::secrete_nb_localvar(Localsvector2d &locals, std::string l_name,
                                int x_pos, int y_pos, double sec_val) {

  if(locals.contents.count(l_name) < 1) {
    throw missing_variable(l_name);
  }

  int x_max =
      std::min(x_pos + 1, (int)(locals.contents[l_name]->values.size()) - 1);
  int x_min = std::max(0, x_pos - 1);
  int y_max =
      std::min(y_pos + 1, (int)(locals.contents[l_name]->values[0].size()) - 1);
  int y_min   = std::max(0, y_pos - 1);
  int num_nbs = (x_max - x_min + 1) * (y_max - y_min + 1) - 1;
  for(int i = x_min; i <= x_max; i++) {

    for(int j = y_min; j <= y_max; j++) {

      if(not(i == x_pos and j == y_pos)) {

        locals.contents[l_name]->values[i][j] =
            locals.contents[l_name]->values[i][j] + (sec_val / num_nbs);
      }
    }
  }
};


// function to turn towards a gradient,
std::pair<int, int> Agent::turn_up_gradient(Localsvector2d &locals,
                                            std::string l_name, int s_range,
                                            std::mt19937 &rng) {

  if(locals.contents.count(l_name) < 1) {
    throw missing_variable(l_name);
  }
  // read out gradient (find patch with maximum vegf value in s_range)
  std::pair<int, int> patch_max = {x, y};
  double maxval = 0;
  int x_max     = (int)locals.contents[l_name]->values.size() - 1;
  int y_max     = (int)locals.contents[l_name]->values[0].size() - 1;
  for(int i = std::max(0, x - s_range); i <= std::min(x + s_range, x_max);
      i++) {

    for(int j = std::max(0, y - s_range); j <= std::min(y + s_range, y_max);
        j++) {

      if(locals.contents[l_name]->values[i][j] > maxval) {

        patch_max.first  = i;
        patch_max.second = j;
        maxval           = locals.contents[l_name]->values[i][j];
      } else if(locals.contents[l_name]->values[i][j] == maxval) {

        std::uniform_int_distribution<> dis(1, 6);
        int check = dis(rng);
        if(check > 3) {
          patch_max.first  = i;
          patch_max.second = j;
          maxval           = locals.contents[l_name]->values[i][j];
        }
      }
    }
  }

  // determine movement direction
  int x_step = 0;
  if(patch_max.first > x) {
    x_step = 1;
  } else if(patch_max.first < x) {
    x_step = -1;
  }

  int y_step = 0;
  if(patch_max.second > y) {
    y_step = 1;
  } else if(patch_max.second < y) {
    y_step = -1;
  }

  std::pair<int, int> to_return = {x_step, y_step};
  return to_return;
};


// turn left or right a determined direction.
std::pair<int, int> Agent::turn(int x_step, int y_step, int clockwise) {

  std::pair<int, int> to_ret = {x_step, y_step};
  if(clockwise == 1) {
    to_ret = {std::min(std::max(-1, -y_step + x_step), 1),
              std::min(std::max(-1, x_step + y_step), 1)};
  } else {
    to_ret = {std::min(std::max(-1, y_step + x_step), 1),
              std::min(std::max(-1, -x_step + y_step), 1)};
  }

  return to_ret;
};


// turn randomly.
std::pair<int, int> Agent::wiggle() {
  std::mt19937 g(static_cast<unsigned int>(time(0)));  // prepare randomness
  std::uniform_int_distribution<> dis(-1, 1);
  int x_step = dis(g);
  int y_step = dis(g);
  return std::pair<int, int>(x_step, y_step);
};


// return value of varname in neibhouring agnets.
double Agent::sense_nb_variable(Agentvector2d agentset, std::string varname) {
  double to_ret = 0.0;

  for(std::vector<int>::size_type i = 0; i != neighbours.size(); i++) {

    int nb_id         = neighbours[i];
    Agent *curr_agent = agentset.agents[nb_id];

    if(curr_agent->v.find(varname) != curr_agent->v.end()) {

      int num_nbs = (int)curr_agent->neighbours.size();
      to_ret += curr_agent->v[varname] / num_nbs;
    }
  }
  return to_ret;
};

// substract a value from thy neighbours variable
void Agent::rem_nb_variable(Agentvector2d agentset, std::string varname,
                            double val) {
  double sum_nbs = 0;
  for(std::vector<int>::size_type i = 0; i != neighbours.size(); i++) {

    int nb_id         = neighbours[i];
    Agent *curr_agent = agentset.agents[nb_id];
    if(curr_agent->v.find(varname) != curr_agent->v.end()) {
      sum_nbs += curr_agent->v[varname];
    }
  }
  val = std::min(val, sum_nbs);
  if(sum_nbs > 0) {

    for(std::vector<int>::size_type i = 0; i != neighbours.size(); i++) {

      int nb_id         = neighbours[i];
      Agent *curr_agent = agentset.agents[nb_id];

      if(curr_agent->v.find(varname) != curr_agent->v.end()) {

        curr_agent->v[varname] -= val * curr_agent->v[varname] / sum_nbs;
      }
    }
  }
};
