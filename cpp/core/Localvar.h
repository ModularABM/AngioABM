////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef LOCALVAR_H
#define LOCALVAR_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include "Globalsvector.h"

// forward declarations to resolve circ. dep.
class Localsvector2d;
class Agentvector2d;


// Localvar, base class for local variables
class Localvar {
public:
  std::string name;
  std::vector<std::vector<double>> values;

  // ___________________________________________________________________________
  // constructors:
  Localvar(std::string initname, std::vector<std::vector<double>> init_values);
  Localvar();


  // ___________________________________________________________________________
  // member functions:

  // virtual destructor
  virtual ~Localvar() {};

  // empty update-template
  virtual void update(Globalsvector &globals, Localsvector2d &locals,
                      Agentvector2d &agents, double t, int t_scale);

  // for on screen output
  virtual void output_state(std::ostream &target = std::cout, double t = 0);

  // output for importing into R data.frame (x,y,t,type,varname, varvalue)
  virtual void output_datatable(std::ostream &target = std::cout, double t = 0);


  // finite difference diffusion function
  void diffuse_findiff(double diff_D, double step_size, double grid_size,
                       int idim, int jdim);

  // simple degradation function
  void degrade(double deg_rate, int idim, int jdim);

  // increase a local variable in some rectangle
  void increase_rect(std::pair<int, int> start_coord,
                     std::pair<int, int> end_coord, double sourceflux);

  // increase values in a vector of coordinates
  void increase_coords(std::vector<std::vector<double>> source,
                                 double stepfactor);

  // increase values in coordainte vector by difference to reference value
  void increase_coords_diff(std::vector<std::vector<double>> source,
                            double stepfactor, double ref_val);

  // naive diffusion function
  void diffuse_naive(double diffusion_rate, int idim, int jdim,
                     Agentvector2d &agents);

  //initialize
  virtual void initialize(Globalsvector globals, Localsvector2d locals,
                         Agentvector2d agents, double t, int t_scale);

  // diffusion via alternating direction explicit scheme
  void diffuse_ade(double diff_D, double step_size, double grid_size,
                   int idim, int jdim, double boundary_v);
};

#endif
