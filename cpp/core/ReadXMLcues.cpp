#include "ReadXMLcues.h"

// chop string of <delimeter> separated integers into vector of integers
std::vector<int> split2int(std::string str, char delimiter) {

  std::vector<int> internal;
  std::stringstream ss(str);  // Turn the string into a stream.
  std::string tok;

  while(getline(ss, tok, delimiter)) {
    int token = stoi(tok);
    internal.push_back(token);
  }
  return internal;
};


// read global variables
Globalsvector read_globals(boost::property_tree::ptree pt) {

  std::map<std::string, double> globals;
  boost::property_tree::ptree xml_globals = pt.get_child("model.globals");
  int iter                                = 0;
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, xml_globals) {

    iter += 1;
    boost::property_tree::ptree attributes = v.second;
    std::string name =
        attributes.get<std::string>("name", "global_" + std::to_string(iter));
    std::string type = attributes.get<std::string>("type", "global");
    if(type == "global") {

      double value = attributes.get<double>("value", 0.0);
      globals.insert(std::pair<std::string, double>(name, value));
    }
  }
  return Globalsvector(globals);
};


// construct the initial values of a local variable
std::vector<std::vector<double>>
    read_init_local(boost::property_tree::ptree attributes, int idim,
                    int jdim) {

  double init_value = attributes.get<double>("initvalue", 0.0);
  std::vector<std::vector<double>> inits(idim,
                                         std::vector<double>(jdim, init_value));

  // if there is a child called initminvalue -> construct initial gradient
  boost::optional<boost::property_tree::ptree &> child =
      attributes.get_child_optional("initminvalue");
  if(child) {

    double init_min_value = attributes.get<double>("initminvalue", 0.0);
    double init_max_value = attributes.get<double>("initmaxvalue", 1.0);
    int init_min_row      = attributes.get<int>("initminrow", 0);
    int init_max_row      = attributes.get<int>("initmaxrow", 0);
    inits = constructGradient(init_max_value, init_min_value, init_max_row,
                              init_min_row, idim, jdim);
  }
  return inits;
};


// construct and check the source field of a local variable
std::vector<std::vector<double>>
    read_source_local(boost::property_tree::ptree attributes, int idim,
                      int jdim) {

  // use a matrix to store influxes, able to use combined shapes like this
  std::vector<std::vector<double>> to_return(idim, std::vector<double>(jdim));

  boost::optional<boost::property_tree::ptree &> xml_source_test =
      attributes.get_child_optional("sources");
  if(xml_source_test) {

    boost::property_tree::ptree xml_sources = attributes.get_child("sources");
    // BOOST_FOREACH(const ptree::value_type &v, pt.get_child(path)) {
    // v.first is the name of the child.
    // v.second is the child tree.
    BOOST_FOREACH(boost::property_tree::ptree::value_type &v, xml_sources) {
      // for each source
      boost::property_tree::ptree xml_coord = v.second;
      std::string shape = xml_coord.get<std::string>("shape", "");
      if(shape == "rectangle") {

        int startx    = xml_coord.get<int>("startx", 0);
        int endx      = xml_coord.get<int>("endx", 0);
        int starty    = xml_coord.get<int>("starty", 0);
        int endy      = xml_coord.get<int>("endy", 0);
        double influx = xml_coord.get<double>("influx", 0.0);

        // check that source coordinates are within 0,xlim and 0,ylim
        startx = std::min(std::max(0, startx), idim);
        endx   = std::min(std::max(0, endx + 1), idim);
        starty = std::min(std::max(0, starty), jdim);
        endy   = std::min(std::max(0, endy + 1), jdim);

        for(int i = startx; i < endx; i++) {

          for(int j = starty; j < endy; j++) {

            to_return[i][j] += influx;
          }
        }

      } else if(shape == "point") {
        int x         = xml_coord.get<int>("x", 0);
        int y         = xml_coord.get<int>("y", 0);
        double influx = xml_coord.get<double>("influx", 0.0);

        // check that x,y are inside limits
        x = std::min(std::max(0, x), idim - 1);
        y = std::min(std::max(0, y), jdim - 1);

        to_return[x][y] += influx;

      } else if(shape == "circular") {
        int startx    = xml_coord.get<int>("start", 0);
        startx        = std::max(0, startx);
        int endx      = xml_coord.get<int>("end", 0);
        endx          = std::min(idim, endx + 1);
        int q         = (endx - startx) / 4;  // ceil((endx - startx) / 4);
        double influx = xml_coord.get<double>("influx", 0.0);

        for(int i = startx; i < endx + 1; i++) {

          for(int j = startx; j < endx + 1; j++) {

            if(j < i + endx - (q + startx) and
               j - startx > (q + startx) - (i) and
               i - 1 < j + endx - (q + startx) - 1 and
               i + j < 2 * startx + 2 * (endx - startx) - q) {

              to_return[i][j] += influx;
            }
          }
        }
      }
    }
  }
  return to_return;
};


// function to read local variables
Localsvector2d read_locals(boost::property_tree::ptree pt, int idim, int jdim) {

  std::vector<std::string> li_names;
  std::vector<double> li_vals;

  Localsvector2d locals = Localsvector2d(li_names, idim, jdim, li_vals);
  boost::property_tree::ptree xml_locals = pt.get_child("model.locals");
  int iter                               = 0;

  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, xml_locals) {

    iter += 1;
    boost::property_tree::ptree attributes = v.second;
    std::string name =
        attributes.get<std::string>("name", "local_" + std::to_string(iter));
    std::string type = attributes.get<std::string>("type", "Localvar");

    if(type == "NaivediffusionWS_lv") {

      std::vector<std::vector<double>> inits =
          read_init_local(attributes, idim, jdim);

      double diffusion_rate   = attributes.get<double>("diffusion_rate", 0.0);
      double degradation_rate = attributes.get<double>("degradation_rate", 0.0);

      std::vector<std::vector<double>> source =
          read_source_local(attributes, idim, jdim);

      NaivediffusionWS_lv *ndWS = new NaivediffusionWS_lv(
          name, inits, diffusion_rate, degradation_rate, source);
      locals.insert(std::pair<std::string, NaivediffusionWS_lv *>(name, ndWS));
    }


    else if(type == "FinDiffDiffusion_lv") {

      std::vector<std::vector<double>> inits =
          read_init_local(attributes, idim, jdim);

      double diffusion_coeff  = attributes.get<double>("diffusion_coeff", 0.0);
      double degradation_rate = attributes.get<double>("degradation_rate", 0.0);

      std::vector<std::vector<double>> source =
          read_source_local(attributes, idim, jdim);

      FinDiffDiffusion_lv *ndWS = new FinDiffDiffusion_lv(
          name, inits, diffusion_coeff, degradation_rate, source);
      locals.insert(std::pair<std::string, FinDiffDiffusion_lv *>(name, ndWS));
    }

    else if(type == "FinDiffDiffusionDS_lv") {

      std::vector<std::vector<double>> inits =
          read_init_local(attributes, idim, jdim);

      double diffusion_coeff  = attributes.get<double>("diffusion_coeff", 0.0);
      double degradation_rate = attributes.get<double>("degradation_rate", 0.0);
      double ref_val          = inits[0][0];

      std::vector<std::vector<double>> source =
          read_source_local(attributes, idim, jdim);

      FinDiffDiffusionDS_lv *ndWS = new FinDiffDiffusionDS_lv(
          name, inits, diffusion_coeff, degradation_rate, source, 1.0, 1.0,
          ref_val);
      locals.insert(
          std::pair<std::string, FinDiffDiffusionDS_lv *>(name, ndWS));
    }

    else if(type == "ADEDiffDiffusion_lv") {

      std::vector<std::vector<double>> inits =
          read_init_local(attributes, idim, jdim);

      double diffusion_coeff  = attributes.get<double>("diffusion_coeff", 0.0);
      double degradation_rate = attributes.get<double>("degradation_rate", 0.0);
      double boundary_val     = attributes.get<double>("boundary_val", 0.0);

      std::vector<std::vector<double>> source =
          read_source_local(attributes, idim, jdim);

      ADEDiffDiffusion_lv *ndWS = new ADEDiffDiffusion_lv(
          name, inits, diffusion_coeff, degradation_rate, source, 1.0, 1.0,
          boundary_val);
      locals.insert(std::pair<std::string, ADEDiffDiffusion_lv *>(name, ndWS));
    }


    else if(type == "svegfr1_lv") {

      std::vector<std::vector<double>> inits =
          read_init_local(attributes, idim, jdim);

      double diffusion_coeff  = attributes.get<double>("diffusion_coeff", 0.0);
      double degradation_rate = attributes.get<double>("degradation_rate", 0.0);
      double binding_k_val    = attributes.get<double>("binding_constant", 0.0);

      std::vector<std::vector<double>> source =
          read_source_local(attributes, idim, jdim);
      svegfr1_lv *ndWS =
          new svegfr1_lv(name, inits, diffusion_coeff, degradation_rate, source,
                         binding_k_val, 1.0, 1.0);
      locals.insert(std::pair<std::string, svegfr1_lv *>(name, ndWS));
    }


    else if(type == "svegfr1bound_lv") {

      std::vector<std::vector<double>> inits =
          read_init_local(attributes, idim, jdim);

      double diffusion_coeff  = attributes.get<double>("diffusion_coeff", 0.0);
      double degradation_rate = attributes.get<double>("degradation_rate", 0.0);
      double diss_k_val       = attributes.get<double>("diss_constant", 0.0);

      std::vector<std::vector<double>> source =
          read_source_local(attributes, idim, jdim);
      svegfr1bound_lv *ndWS =
          new svegfr1bound_lv(name, inits, diffusion_coeff, degradation_rate,
                              source, diss_k_val, 1.0, 1.0);
      locals.insert(std::pair<std::string, svegfr1bound_lv *>(name, ndWS));
    }


    else if(type == "Localvar") {

      std::vector<std::vector<double>> initvec(idim,
                                               std::vector<double>(jdim, 0));

      // if there is a child initvalue -> set initial value
      boost::optional<boost::property_tree::ptree &> child =
          attributes.get_child_optional("initvalue");
      if(child) {

        double initval = attributes.get<double>("initvalue", 0.0);
        initvec        = std::vector<std::vector<double>>(
            idim, std::vector<double>(jdim, initval));
      }

      // loop over all coordinates and try to read a value f
      for(int i = 0; i < idim; i++) {

        for(int j = 0; j < jdim; j++) {

          std::string curr_pos = std::to_string(i) + "_" + std::to_string(j);
          boost::optional<boost::property_tree::ptree &> pos_child =
              attributes.get_child_optional(curr_pos);
          if(pos_child) {

            double pos_val = attributes.get<double>(curr_pos, 0.0);
            initvec[i][j]  = pos_val;
          }
        }
      }

      // prepare local variable and insert
      Localvar *lv = new Localvar(name, initvec);
      locals.insert(std::pair<std::string, Localvar *>(name, lv));
    }
  }
  return locals;
};


// seed agents
template <typename T>
void seed_agents(int idim, int jdim, T m_agent,
                 boost::property_tree::ptree attributes, Agentvector2d &agents,
                 int noisy, std::mt19937 &rng) {


  // prepare randomness
  std::mt19937 g(static_cast<unsigned int>(time(0)));
  std::uniform_int_distribution<int> x_dis(0, idim - 1);
  std::uniform_int_distribution<int> y_dis(0, jdim - 1);

  boost::optional<boost::property_tree::ptree &> rectangle =
      attributes.get_child_optional("startx");
  boost::optional<boost::property_tree::ptree &> circle =
      attributes.get_child_optional("start_circ");
  boost::optional<boost::property_tree::ptree &> skip =
      attributes.get_child_optional("skipnum");

  // if an element startx is given, seed in rectangular area
  if(rectangle) {

    int startx = attributes.get<int>("startx", 0);
    int endx   = attributes.get<int>("endx", 0);
    int starty = attributes.get<int>("starty", 0);
    int endy   = attributes.get<int>("endy", 0);

    int skipped = 0;
    int skipmax = attributes.get<int>("skipnum", 0);


    for(int i = std::max(0, startx); i <= std::min(endx, idim - 1); ++i) {

      for(int j = std::max(0, starty); j <= std::min(endy, jdim - 1); ++j) {
        if(skip and skipped < skipmax) {
          std::uniform_int_distribution<int> dice(0, 100);

          int intdice = dice(g);
          double fthreshold =
              (((double)skipmax + 1.0) / ((endx - startx) * (endy - starty))) *
              100.0;


          if(intdice < fthreshold) {
            ++skipped;
            continue;
          }
        }

        if(noisy == 0) {
          agents.template add_agent_kid<T>(i, j, m_agent);
        } else if(noisy == 1) {
          agents.template add_agent_noisy<T>(i, j, m_agent, rng);
        } else if(noisy == 2) {
          agents.template add_agent_noisykid<T>(i, j, m_agent, rng);
        }
      }
    }

  }

  // if an element start_circ is given, seed in a 'circular area'
  else if(circle) {
    int startx = attributes.get<int>("start_circ", 0);
    startx     = std::max(0, startx);
    int endx   = attributes.get<int>("end_circ", 0);
    endx       = std::min(idim, endx);
    int q      = static_cast<int>(ceil((endx - startx) / 4));
    for(int i = startx; i < endx + 1; i++) {
      for(int j = startx; j < endx + 1; j++) {

        if(j < i + endx - (q + startx) and j - startx > (q + startx) - (i) and
           i - 1 < j + endx - (q + startx) - 1 and
           i + j < 2 * startx + 2 * (endx - startx) - q) {

          if(noisy == 0) {
            agents.template add_agent_kid<T>(i, j, m_agent);
          } else if(noisy == 1) {
            agents.template add_agent_noisy<T>(i, j, m_agent, rng);
          } else if(noisy == 2) {
            agents.template add_agent_noisykid<T>(i, j, m_agent, rng);
          }
        }
      }
    }
  }


  // if not, seed them at random locations
  else {

    int num_agents = attributes.get<int>("numagents", 0);
    for(int i = 0; i < std::min(num_agents, idim * jdim); ++i) {
      // std::cout << "seeding agent " << i << " of " << num_agents <<
      // std::endl;
      // std::uniform_int_distribution<int> dice(0, 100);

      //     int intdice = dice(g);

      int randx = x_dis(g);
      // std::cout << "seeding agent, rolled x dice " << randx << std::endl;
      int randy = y_dis(g);
      // std::cout << "seeding agent, rolled y dice " << randy << std::endl;
      while(!agents.check_free(randx, randy)) {

        randx = x_dis(g);
        randy = y_dis(g);
      }
      // std::cout << "\t at random position: " << randx << ", " << randy
      //           << ", with noisy=" << noisy << std::endl;

      if(noisy == 0) {
        agents.template add_agent_kid<T>(randx, randy, m_agent);
      } else if(noisy == 1) {
        agents.template add_agent_noisy<T>(randx, randy, m_agent, rng);
      } else if(noisy == 2) {
        agents.template add_agent_noisykid<T>(randx, randy, m_agent, rng);
      }
    }
  }
};


// read agents
Agentvector2d read_agents(boost::property_tree::ptree pt, int idim, int jdim,
                          std::mt19937 &rng) {

  // std::cout << "\treading agents\n";
  Agentvector2d agents = Agentvector2d(idim, jdim);

  boost::property_tree::ptree xml_agents = pt.get_child("model.agents");
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, xml_agents) {

    std::unordered_map<std::string, double> v_inits;
    std::unordered_map<std::string, double> p_inits;


    boost::property_tree::ptree attributes = v.second;
    std::string type = attributes.get<std::string>("type", "Agent");
    int sampling     = attributes.get<int>("sample", 0);

    // std::cout << "test: " << attributes.data() << std::endl;
    // BOOST_FOREACH(boost::property_tree::ptree::value_type &w, attributes) {
    //   boost::property_tree::ptree vars_contents = w.second;
    //   // std::cout << "test2: " << vars_contents.get<std::string>() <<
    //   // std::endl;
    //   std::cout << "test2: " << w.first << ": "
    //             << attributes.get<std::string>(w.first) << std::endl;
    // }

    // check for <variables> and read them into v_inits and headers
    boost::optional<boost::property_tree::ptree &> vars =
        attributes.get_child_optional("variables");
    if(vars) {
      boost::property_tree::ptree vars = attributes.get_child("variables");
      BOOST_FOREACH(boost::property_tree::ptree::value_type &w, vars) {
        agents.headers.push_back(w.first);  // extend header
        if(sampling == 0) {
          v_inits.insert({w.first, vars.get<double>(w.first, 0.0)});
        } else if(sampling == 1) {
          // read parameters as string and insert them into the
          // agentvector.spec_dists
          std::string fullparams = vars.get<std::string>(w.first, "");

          std::vector<double> params = split2d(fullparams, ',');

          agents.spec_dists[type][w.first] = params;

          // insert a sampled value into v_inits to make it complete
          double sampled = sample_rand(params, rng);

          v_inits.insert({w.first, sampled});
        }
      }
    }

    // check for <parameters> and read them into v_inits and headers
    boost::optional<boost::property_tree::ptree &> pars =
        attributes.get_child_optional("parameters");
    if(pars) {
      boost::property_tree::ptree pars = attributes.get_child("parameters");
      BOOST_FOREACH(boost::property_tree::ptree::value_type &w, pars) {
        if(sampling == 0) {
          p_inits.insert({w.first, pars.get<double>(w.first, 0.0)});
        } else if(sampling == 1) {
          // read parameters as string and insert them into the
          // agentvector.spec_dists
          std::string fullparams = pars.get<std::string>(w.first, "");

          std::vector<double> params = split2d(fullparams, ',');

          agents.par_dists[type][w.first] = params;

          // insert a sampled value into v_inits to make it complete
          double sampled = sample_rand(params, rng);

          p_inits.insert({w.first, sampled});
        }
      }
    }

    // create agent depending on type
    if(type == "DiffEqEC_agent") {
      DiffEqEC_agent m_agent = DiffEqEC_agent(0, 0, 0, &v_inits, &p_inits);

      seed_agents<DiffEqEC_agent>(idim, jdim, m_agent, attributes, agents,
                                  sampling, rng);
    }

    else if(type == "VEGFR1_agent") {

      VEGFR1_agent m_agent = VEGFR1_agent(0, 0, 0, &v_inits, &p_inits);
      seed_agents<VEGFR1_agent>(idim, jdim, m_agent, attributes, agents,
                                sampling, rng);
    } else if(type == "VEGFR1v2_agent") {

      VEGFR1v2_agent m_agent = VEGFR1v2_agent(0, 0, 0, &v_inits, &p_inits);

      seed_agents<VEGFR1v2_agent>(idim, jdim, m_agent, attributes, agents,
                                  sampling, rng);
    } else if(type == "MechEC_agent") {

      MechEC_agent m_agent = MechEC_agent(0, 0, 0, &v_inits, &p_inits);
      seed_agents<MechEC_agent>(idim, jdim, m_agent, attributes, agents,
                                sampling, rng);
    }
  }
  return agents;
};


// read xml file and construct ABModel2D from it
ABModel2d constructModel(std::string filename, int verbose, std::mt19937 &rng) {

  // Create an empty property tree object
  using boost::property_tree::ptree;
  ptree pt;

  // Load the XML file into the property tree.
  read_xml(filename, pt);


  // general
  int idim    = pt.get<int>("model.dimensions.xval", 0);
  int jdim    = pt.get<int>("model.dimensions.yval", 0);
  int t_max   = pt.get<int>("model.dimensions.tval", 0);
  int t_scale = pt.get<int>("model.dimensions.tscale", 1);
  if(verbose > 0) {
    std::cout << "generate model with idim, jdim, t_max, tscale " << idim
              << ", " << jdim << ", " << t_max << ", " << t_scale << "\n";
  }
  // globals
  Globalsvector globals = read_globals(pt);

  // locals
  Localsvector2d locals = read_locals(pt, idim, jdim);

  // agents
  // std::cout << "getting agents\n";
  Agentvector2d agents = read_agents(pt, idim, jdim, rng);

  // std::cout << "got agents" << std::endl;
  // declaration of output details
  int output_type_val = 1;
  std::vector<int> output_timepoints_val(t_max + 1);  // = {0, t_max, 1};
  std::iota(output_timepoints_val.begin(), output_timepoints_val.end(), 0);

  // std::cout << "output_timepoints_val contains" << std::endl;
  // for(auto const &value : output_timepoints_val) {
  //   std::cout << value << ",";
  // }
  // std::cout << std::endl;

  // std::generate(output_timepoints_val.begin(),
  //               output_timepoints_val.end(), [n = 0]() mutable { return n++;
  //               });

  std::string output_basename_val        = "test";
  std::string output_format_val          = "compact";
  boost::property_tree::ptree xml_output = pt.get_child("model.output");
  output_type_val     = xml_output.get<int>("output_type", 0);
  output_basename_val = xml_output.get<std::string>("basename", "__x__");
  output_format_val   = xml_output.get<std::string>("output_format", "compact");

  // get the timepoints
  boost::optional<ptree &> child_tps =
      xml_output.get_child_optional("timepoints");
  if(child_tps) {

    std::string timepoints_string = xml_output.get<std::string>("timepoints");
    output_timepoints_val         = split2int(timepoints_string, ',');
    output_type_val               = 0;
  }
  boost::optional<ptree &> child_ts =
      xml_output.get_child_optional("timesequence");
  if(child_ts) {

    std::string timepoints_string = xml_output.get<std::string>("timesequence");
    output_timepoints_val         = split2int(timepoints_string, ',');
  }
  if(verbose > 0) {
    std::cout << "got output"
              << "\n";
  }

  // ABModel2d to_return = ABModel2d(idim, jdim);
  ABModel2d to_return = ABModel2d(
      idim, jdim, globals, locals, agents, t_max, t_scale, output_type_val,
      output_timepoints_val, output_basename_val, output_format_val);

  return to_return;
};


// construct initial gradients for localvariables
std::vector<std::vector<double>> constructGradient(double maxval, double minval,
                                                   int maxrow, int minrow,
                                                   int idim, int jdim) {

  std::vector<std::vector<double>> retval(idim, std::vector<double>(jdim, 0));
  for(int j = std::min(jdim, maxrow + 1); j > std::max(0, minrow); j--) {

    for(int i = 0; i < idim; i++) {

      retval[i][j] =
          maxval - (maxval - minval) / (maxrow - minrow) * (jdim - j);
    }
  }
  return retval;
};


std::vector<double> split2d(const std::string &s, char delim) {
  std::vector<double> result;
  std::stringstream ss(s);
  std::string item;
  while(std::getline(ss, item, delim)) {
    result.push_back(stod(item));
  }
  return (result);
};

double sample_rand(std::vector<double> dist_params, std::mt19937 &rng) {

  std::uniform_real_distribution<> dis(0, 1);
  double to_ret = -1.0;

  if(dist_params[0] == 0.0) {
    // use uniform distribution
    std::uniform_real_distribution<> dis(dist_params[1], dist_params[2]);
    to_ret = dis(rng);

  } else if(dist_params[0] == 1.0) {
    // use poisson distribution
    std::poisson_distribution<> dis(dist_params[1]);
    to_ret = dis(rng);
  } else if(dist_params[0] == 2.0) {
    // use exponential distribution
    std::exponential_distribution<> dis(dist_params[1]);
    to_ret = dis(rng);
  } else if(dist_params[0] == 3.0) {

    // use normal distribution
    std::normal_distribution<> dis(dist_params[1], dist_params[2]);
    to_ret = dis(rng);
  } else if(dist_params[0] == 4.0) {
    // use lognormal dist! LOGTRANSFORM PARAMETERS HERE!
    double m  = dist_params[1];
    double v  = dist_params[2];
    double mu = log(m / pow(1 + v / pow(m, 2), 0.5));
    double sd = m * pow(log(1 + v / pow(m, 2)), 1);
    std::lognormal_distribution<> dis(mu, sd);
    to_ret = dis(rng);
  } else if(dist_params[0] == 99.0) {
    // use value indicated
    to_ret = dist_params[1];
  }
  return (to_ret);
};
