////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef AGENTVECTOR_H
#define AGENTVECTOR_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <random>
#include <algorithm>  // using, at least, std::max_element
#include <ctime>
#include <typeinfo>
#include <boost/functional/hash.hpp>
#include "Agent.h"
#include "Localsvector.h"
#include "Globalsvector.h"
#include "sys/time.h"

// struct for exception throwing
struct out_of_bounds {

  std::string varname;

  out_of_bounds() { varname = "NA"; };
  out_of_bounds(std::string varname_value) { varname = varname_value; };
};


// forward declarations to resolve circ. dep.
class Agent;
class DiffEqEC_agent;
class DNcis_agent;
class DNcisAN_agent;
class DNcisV2_agent;
class VEGFR1_agent;


// Agentvector class holds a map of agents plus auxiliary maps of coordinates
class Agentvector2d {
public:
  std::map<int, Agent *> agents;  // ids -> agents
  /* unordered_map<pair<int, int>, Agent *, boost::hash<pair<int, int>>> */
  /*     coords; */
  std::vector<std::vector<Agent *>>
      coords;                  // (x,y) -> id; to find agents and neighbours
  std::pair<int, int> xy_max;  // so that it knows its boundaries
  std::mt19937::result_type
      av_seed;  // to be used in all downstream functions/classes
  std::unordered_map<std::string, std::unordered_map<std::string,
    std::vector<double>>> par_dists;
  
  std::unordered_map<std::string, std::unordered_map<std::string,
  std::vector<double>>> spec_dists;

  std::vector<std::string> headers;
    

  // ___________________________________________________________________________
  // constructors:
  Agentvector2d()
      : agents(std::map<int, Agent *>()),
        coords(std::vector<std::vector<Agent *>>()), xy_max(0, 0){};
  Agentvector2d(int x_max, int y_max);


  // ___________________________________________________________________________
  // member functions:

  // get IDs of Agents
  std::vector<int> retrieve_ids();

  // check if coordinate position is free
  bool check_free(int x_check, int y_check);

  // check if outside the xy_max
  bool check_outside(int x_check, int y_check);

  // add agent that is a copy of argument new_agent to x,y, assign new id automatic
  template <typename T> void add_agent_kid(int x, int y, T new_agent) {

    if(x > xy_max.first or y > xy_max.second or x < 0 or y < 0) {
      throw out_of_bounds("new Agent");
    }

    std::vector<int> curr_ids = retrieve_ids();
    int id_new                = 1;
    if(curr_ids.size() > 0) {
      id_new = *max_element(curr_ids.begin(), curr_ids.end()) + 1;
    }

    T *insert_agent = new T(new_agent);

    insert_agent->setid(id_new);
    insert_agent->setpos(x, y);
    agents.insert(std::pair<int, Agent *>(id_new, insert_agent));

    coords[x][y] = insert_agent;

    // update neighbours:
    update_own_neighbours(id_new);
    update_new_neighbours(id_new);
  };


  // add agent that is a resampled copy of new_agent to x,y, assign new id automatic
  template <typename T> void add_agent_noisy(int x, int y, T new_agent,
                                             std::mt19937 &rng) {

    if(x > xy_max.first or y > xy_max.second or x < 0 or y < 0) {
      throw out_of_bounds("new Agent");
    }

    std::vector<int> curr_ids = retrieve_ids();
    int id_new                = 1;
    if(curr_ids.size() > 0) {
      id_new = *max_element(curr_ids.begin(), curr_ids.end()) + 1;
    }

    /* std::cout<<"Agentvector::add_agent_noisy 3"<<std::endl; */

    T *insert_agent = new T(new_agent);

    insert_agent->setid(id_new);
    insert_agent->setpos(x, y);

    /* std::cout<<"Agentvector::add_agent_noisy 4"<<std::endl; */


    // sample from par_dists and speci_dists
    // re-populate a unordered_map<string, double> based on distributions given
    /* std::cout<<"\t\tsampling agents' variables with "<<insert_agent->type<<"\n"; */
    std::unordered_map<std::string, double> new_v=
      sample_set(insert_agent->v,
               spec_dists[insert_agent->type], rng);
    /* std::cout<<"\t\tinserting sampled variables into agent"; */
    /* std::cout<<"Agentvector::add_agent_noisy 5"<<std::endl; */

    insert_agent->v = new_v;
    
    /* std::cout<<"Agentvector::add_agent_noisy 6"<<std::endl; */
    /* std::cout<<"\t\tsampling agents' parameters\n"; */
    std::unordered_map<std::string, double> new_p=
      sample_set(insert_agent->p,
               par_dists[insert_agent->type], rng);
    /* std::cout<<"Agentvector::add_agent_noisy 7"<<std::endl; */

    /* std::cout<<"\t\tinserting sampled parameters into agent"; */
    insert_agent->p = new_p;

    /* std::cout<<"Agentvector::add_agent_noisy 8"<<std::endl; */

    agents.insert(std::pair<int, Agent *>(id_new, insert_agent));
    /* std::cout<<"Agentvector::add_agent_noisy 9"<<std::endl; */

    coords[x][y] = insert_agent;
    /* std::cout<<"Agentvector::add_agent_noisy 10"<<std::endl; */

    // update neighbours:
    update_own_neighbours(id_new);
    /* std::cout<<"Agentvector::add_agent_noisy 11"<<std::endl; */
    update_new_neighbours(id_new);
    /* std::cout<<"Agentvector::add_agent_noisy 12"<<std::endl; */

  };

  // add agent that has mean values of new_agent and as sampled from template to x,y,
  //  assign new id automatic *
  template <typename T> void add_agent_noisykid(int x, int y, T new_agent,
                                                std::mt19937 &rng) {

    if(x > xy_max.first or y > xy_max.second or x < 0 or y < 0) {
      throw out_of_bounds("new Agent");
    }

    std::vector<int> curr_ids = retrieve_ids();
    int id_new                = 1;
    if(curr_ids.size() > 0) {
      id_new = *max_element(curr_ids.begin(), curr_ids.end()) + 1;
    }

    T *insert_agent = new T(new_agent);

    insert_agent->setid(id_new);
    insert_agent->setpos(x, y);

    // sample from par_dists and speci_dists
    // and compute mean with actual values
    /* std::cout<<"\t\tsampling agents' variables with "<<insert_agent->type<<"\n"; */
    std::unordered_map<std::string, double> new_v=
      samplemean_set(insert_agent->v,
               spec_dists[insert_agent->type], rng);
    /* std::cout<<"\t\tinserting sampled variables into agent"; */
    insert_agent->v = new_v;
    
    /* std::cout<<"\t\tsampling agents' parameters\n"; */
    std::unordered_map<std::string, double> new_p=
      samplemean_set(insert_agent->p,
               par_dists[insert_agent->type], rng);
    /* std::cout<<"\t\tinserting sampled parameters into agent"; */
    insert_agent->p = new_p;

    
    agents.insert(std::pair<int, Agent *>(id_new, insert_agent));

    coords[x][y] = insert_agent;

    // update neighbours:
    update_own_neighbours(id_new);
    update_new_neighbours(id_new);
  };

  // insert new distributions for an agents parameters
  void insert_pardists(std::string agentname, std::unordered_map<std::string,
                       std::vector<double>> parlist);
  
  // insert new distributions for an agents initial values
  void insert_specdists(std::string agentname, std::unordered_map<std::string,
                                    std::vector<double>> speclist);

  // update neighbourlists for some agent
  void update_own_neighbours(int my_id);

  // move, do all the updating
  int move_agent(int my_id, int x_step, int y_step);

  // helper function, rem self from old neighbours when moving
  void update_old_neighbours(int my_id, std::vector<int> old_neighbours);


  // add self to new neighbours' neighbourlists
  void update_new_neighbours(int my_id);

  // output for importing into R data.frame
  void output_dt(std::ostream &target = std::cout, double timepoint = 0);


  // output for importing into R data.frame
  void output_compact(std::ostream &target = std::cout, double timepoint = 0);

  // update by calling agents' update-fct. synchronicity, rand-order as
  // parameters
  void update(Globalsvector &globals, Localsvector2d &locals,
              Agentvector2d &agentset, double t, int t_scale, int t_intstep,
              int sync, int randorder, std::mt19937 &rng);

  // return size of agentvector
  int size();

  // delete contents
  void rm();

  // get the total of a variable over all agents
  double total_var(std::string v_name);

  // re-populate a unordered_map<string, double> based on distributions given
std::unordered_map<std::string, double>
  sample_set(std::unordered_map<std::string, double> existing,
               std::unordered_map<std::string, std::vector<double>>
             dists, std::mt19937 &rng);

// re-populate a unordered_map<string, double> based on mean of value and dists
std::unordered_map<std::string, double>
  samplemean_set(std::unordered_map<std::string, double> existing,
               std::unordered_map<std::string, std::vector<double>>
             dists, std::mt19937 &rng);
// initialize
 void initialize(Globalsvector globals, Localsvector2d locals,
                 Agentvector2d agentset, double t, int t_scale);
};


#endif
