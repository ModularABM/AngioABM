////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef READXMLCUES_H
#define READXMLCUES_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <random>
#include <boost/optional/optional.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>

#include "ABModel.h"
#include "Localsvector.h"
#include "../localtypes/NaivediffusionWS_lv.h"
#include "../localtypes/FinDiffDiffusion_lv.h"
#include "../localtypes/FinDiffDiffusionDS_lv.h"
#include "../localtypes/ADEDiffDiffusion_lv.h"
#include "../localtypes/svegfr1_lv.h"
#include "../localtypes/svegfr1bound_lv.h"
#include "../agenttypes/DiffEqEC_agent.h"
#include "../agenttypes/VEGFR1_agent.h"
#include "../agenttypes/VEGFR1v2_agent.h"
#include "../agenttypes/MechEC_agent.h"


// ___________________________________________________________________________
// functions to construct an ABModel2D from an xml file
// ___________________________________________________________________________

// chop string of <delimeter> separated integers into vector of integers
std::vector<int> split2int(std::string str, char delimiter);

// construct ABModel from xml-file
ABModel2d constructModel(std::string filename, int verbose, std::mt19937 &rng);

// construct initial gradients for localvariables
std::vector<std::vector<double>> constructGradient(double maxval, double minval,
                                                   int maxrow, int minrow,
                                                   int idim, int jdim);

// read global valriables
Globalsvector read_globals(boost::property_tree::ptree pt);

// read local variables
Localsvector2d read_locals(boost::property_tree::ptree pt, int idim, int jdim);

// construct the initial values
std::vector<std::vector<double>>
    read_init_local(boost::property_tree::ptree attributes, int idim, int jdim);

// construct and check the source field of a local variable
std::vector<std::vector<double>>
    read_source_local(boost::property_tree::ptree attributes, int idim,
                      int jdim);

// read agents
Agentvector2d read_agents(boost::property_tree::ptree pt, int idim, int jdim,
                          std::mt19937 &rng);

// seed agents
template <typename T>
void seed_agents(int idim, int jdim, T m_agent,
                 boost::property_tree::ptree attributes, Agentvector2d &agents,
                 int noisy, std::mt19937 &rng);

std::vector<double> split2d(const std::string &s, char delim);

double sample_rand(std::vector<double> dist_params, std::mt19937 &rng);

#endif
