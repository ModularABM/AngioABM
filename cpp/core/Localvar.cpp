#include "Localvar.h"

// Implementation of Localvar base class


// basic constructor
Localvar::Localvar(std::string initname,
                   std::vector<std::vector<double>> init_values) {

  name   = initname;
  values = init_values;
};


// empty constructor
Localvar::Localvar() {

  name   = "";
  values = std::vector<std::vector<double>>();
};


// empty placeholder for inheritance
void Localvar::update(Globalsvector &globals, Localsvector2d &locals,
                      Agentvector2d &agents, double t, int t_scale){};


// output for reading on screen
void Localvar::output_state(std::ostream &target, double t) {

  target << "\n" << name << " @ " << t;
  for(unsigned i = 0; i < values.size(); i++) {

    for(unsigned j = 0; j < values[i].size(); j++) {

      target << values[i][j] << ",";
    }
    target << "\n";
  }
};


// output for importing into R data.frame (x,y,t,type,varname, varvalue)
void Localvar::output_datatable(std::ostream &target, double t) {

  std::string output_string = "";
  for(unsigned i = 0; i < values.size(); i++) {

    for(unsigned j = 0; j < values[i].size(); j++) {
      output_string += std::to_string(i) + "," + std::to_string(j) + "," +
                       std::to_string(t) + ",local," + name + "," +
                       std::to_string(values[i][j]) + "\n";
    }
  }

  target << output_string;
};


// generic finite difference diffusion function.
// Intended to be used after determining add_steps
void Localvar::diffuse_findiff(double diff_D, double step_size,
                               double grid_size, int idim, int jdim) {

  // copy of self to use for updating
  std::vector<std::vector<double>> old_val(values);
  std::vector<std::vector<double>> new_val(idim,
                                           (std::vector<double>(jdim, 0.0)));

  for(int i = 0; i < idim; i++) {

    for(int j = 0; j < jdim; j++) {

      double d2u_dx2;
      // boundary condition x1
      if(i == 0) {
        d2u_dx2 =
            diff_D * (old_val[i][j] - 2 * old_val[i][j] + old_val[i + 1][j]);
      }
      // boundary condition x2
      else if(i == idim - 1) {
        d2u_dx2 =
            diff_D * (old_val[i - 1][j] - 2 * old_val[i][j] + old_val[i][j]);
      }
      // normal fields
      else {
        d2u_dx2 = diff_D *
                  (old_val[i - 1][j] - 2 * old_val[i][j] + old_val[i + 1][j]);
      }

      double d2u_dy2;
      // boundary condition y1
      if(j == 0) {
        d2u_dy2 =
            diff_D * (old_val[i][j] - 2 * old_val[i][j] + old_val[i][j + 1]);
      }
      // boundary condition y2
      else if(j == jdim - 1) {
        d2u_dy2 =
            diff_D * (old_val[i][j - 1] - 2 * old_val[i][j] + old_val[i][j]);
      }
      // normal fields
      else {
        d2u_dy2 = diff_D *
                  (old_val[i][j - 1] - 2 * old_val[i][j] + old_val[i][j + 1]);
      }

      // put diffusion together
      new_val[i][j] = std::max(0.0, old_val[i][j] + d2u_dx2 + d2u_dy2);

      // insert small fuckup to prevent BIG fuckups
      if(std::isinf(new_val[i][j])) {
        new_val[i][j] = 0;
      }
    }
  }
  values = new_val;
};


// generic degradation function.
void Localvar::degrade(double deg_rate, int idim, int jdim) {

  for(int i = 0; i < idim; i++) {

    for(int j = 0; j < jdim; j++) {

      values[i][j] *= (1 - deg_rate);
    }
  }
};


// generic function to increase values in a rectangle
void Localvar::increase_rect(std::pair<int, int> start_coord,
                             std::pair<int, int> end_coord, double sourceflux) {

  if(sourceflux > 0) {
    for(int i = start_coord.first; i < end_coord.first; i++) {

      for(int j = start_coord.second; j < end_coord.second; j++) {

        values[i][j] += sourceflux;
      }
    }
  }
};


// increase values in a vector of coordinatesw
void Localvar::increase_coords(std::vector<std::vector<double>> source,
                               double stepfactor) {

  for(unsigned i = 0; i < source.size(); i++) {

    for(unsigned j = 0; j < source[i].size(); j++) {

      values[i][j] += source[i][j] / stepfactor;
    }
  }
};


// increase values in coordainte vector by difference to reference value
void Localvar::increase_coords_diff(std::vector<std::vector<double>> source,
                                    double stepfactor, double ref_val) {

  for(unsigned i = 0; i < source.size(); i++) {

    for(unsigned j = 0; j < source[i].size(); j++) {

      values[i][j] += source[i][j] / stepfactor * (-values[i][j] + ref_val);
    }
  }
};


// generic naive diffusion function
void Localvar::diffuse_naive(double diffusion_rate, int idim, int jdim,
                             Agentvector2d &agents) {

  // make a copy of self to use for updating
  std::vector<std::vector<double>> old_val(values);
  std::vector<std::vector<double>> new_val(idim,
                                           (std::vector<double>(jdim, 0.0)));

  for(int i = 0; i < idim; i++) {

    for(int j = 0; j < jdim; j++) {

      if(values[i][j] > 0) {

        // diffuse away from self
        new_val[i][j] += values[i][j] * (1 - diffusion_rate);

        // go through neighbours and add 1/8 of diffusion rate to them
        // count up the non-neighbours and keep that much for self
        int num_nb           = 0;
        double diffusion_val = values[i][j] * diffusion_rate / 8;
        for(int k = std::max(0, i - 1); k < std::min(idim, i + 2); k++) {

          for(int l = std::max(0, j - 1); l < std::min(jdim, j + 2); l++) {

            if(k == i and l == j) {
              continue;
            }

            // diffuse for real neighbours
            if(!agents.coords[k][l]) {
              // if(agents.coords.count(pair<int, int>{k, l}) < 1) {
              new_val[k][l] += diffusion_val;
              num_nb++;
            }
          }
        }
        // keep what didn't go to neighbours
        new_val[i][j] += diffusion_val * (8 - num_nb);  // non_neighbours;
      }
    }
  }

  values = new_val;
};

// initialize (empty template)
void Localvar::initialize(Globalsvector globals, Localsvector2d locals,
                          Agentvector2d agents, double t, int t_scale){};


// diffusion via alternating direction explicit scheme
void Localvar::diffuse_ade(double diff_D, double step_size, double grid_size,
                           int idim, int jdim, double boundary_v) {
  // std::cout << "in diffuse_ade" << std::endl;

  // copy of self to use for updating
  std::vector<std::vector<double>> old_val(values);
  std::vector<std::vector<double>> new_val(idim,
                                           (std::vector<double>(jdim, 0.0)));
  std::vector<std::vector<double>> new_fv(values);
  std::vector<std::vector<double>> new_bv(values);

  // std::cout << "\t1" << std::endl;

  // 1st pass over i,j: fill new_fv:
  for(auto i = 0; i < idim; i++) {
    // std::cout << "\t\t" << i << std::endl;

    for(auto j = 0; j < jdim; j++) {
      // std::cout << "\t\t\t" << j << std::endl;

      double val = 0;
      // sum up terms as in u(t,i+1,j) + u(t+dt,i-1,j) + u(t,i,j+1) + u(t+dt,
      // i,j-1) - 2u(t,i,j)
      // u(t,i+1,j)
      if(i == idim - 1) {
        // val += boundary_v;
        val += new_fv[i][j];
      } else {
        val += new_fv[i + 1][j];
      }
      // u(t+dt,i-1,j)
      if(i == 0) {
        // val += boundary_v;
        val += new_fv[i][j];
      } else {
        val += new_fv[i - 1][j];
      }
      // u(t,i,j+1)
      if(j == jdim - 1) {
        // val += boundary_v;
        val += new_fv[i][j];
      } else {
        val += new_fv[i][j + 1];
      }
      // u(t+dt, i,j-1)
      if(j == 0) {
        // val += boundary_v;
        val += new_fv[i][j];
      } else {
        val += new_fv[i][j - 1];
      }
      // - 2u(t,i,j)
      val -= 2 * new_fv[i][j];
      // finish
      new_fv[i][j] = (val * step_size * diff_D + new_fv[i][j]) /
                     (1 + 2 * diff_D * step_size);
    }
  }
  // std::cout << "\t2" << std::endl;

  // 2nd pass over i,j: fill new_bv:
  for(auto i = idim - 1; i >= 0; i--) {
    for(auto j = jdim - 1; j >= 0; j--) {
      double val = 0;
      // sum up terms as in v(t+dt,i+1,j) + u(t,i-1,j) + u(t+dt,i,j+1) + u(t,
      // i,j-1) - 2u(t,i,j)
      // v(t+dt,i+1,j)
      if(i == idim - 1) {
        // val += boundary_v;
        val += new_bv[i][j];
      } else {
        val += new_bv[i + 1][j];
      }
      // + u(t,i-1,j)
      if(i == 0) {
        // val += boundary_v;
        val += new_bv[i][j];
      } else {
        val += new_bv[i - 1][j];
      }
      // + u(t+dt,i,j+1)
      if(j == jdim - 1) {
        // val += boundary_v;
        val += new_bv[i][j];
      } else {
        val += new_bv[i][j + 1];
      }
      // + u(t, i,j-1)
      if(j == 0) {
        // val += boundary_v;
        val += new_bv[i][j];
      } else {
        val += new_bv[i][j - 1];
      }
      // - 2u(t,i,j)
      val -= 2 * new_bv[i][j];
      // finish
      new_bv[i][j] = (val * step_size * diff_D + new_bv[i][j]) /
                     (1 + 2 * diff_D * step_size);
    }
  }
  // std::cout << "\t3" << std::endl;

  // 3rd pass over i,j: fill new_val:
  for(int i = 0; i < idim; i++) {
    for(int j = 0; j < jdim; j++) {

      // if(i == 0 || i == idim - 1 || j == 0 || j == jdim - 1) {
      //   new_val[i][j] = boundary_v;
      // }
      // else{
      new_val[i][j] = std::max(0.0, (new_bv[i][j] + new_fv[i][j]) / 2);
      // }

      // insert small fuckup to prevent BIG fuckups
      if(std::isinf(new_val[i][j])) {
        new_val[i][j] = 0;
      }
    }
  }
  values = new_val;

  // std::cout << "after diffuse_ade" << std::endl;
};
