////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef LOCALSVECTOR_H
#define LOCALSVECTOR_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <memory>
#include "Agentvector.h"
#include "Localvar.h"
#include "Globalsvector.h"

// forward declarations to resolve circ. dep.
class Localvar;
class Globalsvector;


// Container class for local variables
class Localsvector2d {
public:
  std::map<std::string, Localvar *> contents;

  // ___________________________________________________________________________
  // constructors:
  Localsvector2d() : contents(std::map<std::string, Localvar *>()){};
  Localsvector2d(std::vector<std::string> nameslist, int idim_val,
                 int jdim_val);
  Localsvector2d(std::vector<std::string> nameslist, int idim_val, int jdim_val,
                 std::vector<double> init_values);
  Localsvector2d(std::vector<std::string> nameslist, int idim_val, int jdim_val,
                 std::vector<std::vector<std::vector<double>>> init_values);

  // ___________________________________________________________________________
  // member functions:

  // update
  void update(Globalsvector &globals, Localsvector2d &locals,
              Agentvector2d &agents, double t, int t_scale);

  // R data.frame
  void output_dt(std::ostream &target = std::cout, double t = 0);

  // R data.frame
  void output_compact(std::ostream &target = std::cout, double t = 0);

  // insert a new Localvar
  void insert(std::pair<std::string, Localvar *> new_localvar);

  // delete the contents
  void rm();

  // set all localvars to 0
  void set_field_at(std::pair<int, int> coords, double value);

  // set localvar name to 0 at coords
  void set_variable_at(std::pair<int, int> coords, double value,
                       std::string name);

  // initialize
  void initialize(Globalsvector &globals, Localsvector2d &locals, Agentvector2d &agents, int t,
                int t_scale);
};

#endif
