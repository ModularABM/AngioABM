#include "Localsvector.h"

// Container class for local variables


// initialize with a vector of names and the dimensions
Localsvector2d::Localsvector2d(const std::vector<std::string> nameslist,
                               const int idim_val, const int jdim_val) {

  for(unsigned i = 0; i < nameslist.size(); i++) {

    std::vector<std::vector<double>> zerocontent(idim_val,
                                                 std::vector<double>(jdim_val));
    Localvar *to_insert = new Localvar(nameslist[i], zerocontent);
    std::pair<std::string, Localvar *> insert_this =
        std::pair<std::string, Localvar *>(nameslist[i], to_insert);
    contents.insert(insert_this);
  }
};


// initialize with a vector of names and the dimensions and uniform init values
Localsvector2d::Localsvector2d(std::vector<std::string> nameslist, int idim_val,
                               int jdim_val, std::vector<double> init_values) {

  for(unsigned i = 0; i < nameslist.size(); i++) {

    std::vector<std::vector<double>> zerocontent(
        idim_val, std::vector<double>(jdim_val, init_values[i]));
    Localvar *to_insert = new Localvar(nameslist[i], zerocontent);
    std::pair<std::string, Localvar *> insert_this =
        std::pair<std::string, Localvar *>(nameslist[i], to_insert);
    contents.insert(insert_this);
  }
};


// initialize with a vector of names and the dimensions and prepared init values
Localsvector2d::Localsvector2d(
    std::vector<std::string> nameslist, int idim_val, int jdim_val,
    std::vector<std::vector<std::vector<double>>> init_values) {

  for(unsigned i = 0; i < nameslist.size(); i++) {

    Localvar *to_insert = new Localvar(nameslist[i], init_values[i]);
    std::pair<std::string, Localvar *> insert_this =
        std::pair<std::string, Localvar *>(nameslist[i], to_insert);
    contents.insert(insert_this);
  }
};


// call update-fct of all underlying local variables
void Localsvector2d::update(Globalsvector &globals, Localsvector2d &locals,
                            Agentvector2d &agents, double t, int t_scale) {

  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {

    Localvar *localvarp = iterator->second;
    localvarp->update(globals, locals, agents, t, t_scale);
  }
};


// call output to R data.frame (x,y,t,type,varname, varvalue) for each variable
void Localsvector2d::output_dt(std::ostream &target, double t) {

  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {

    Localvar localvarp = *iterator->second;
    localvarp.output_datatable(target, t);
  }
};


// call output to R data.frame (x,y,t, var1, var2, ...) for each variable
void Localsvector2d::output_compact(std::ostream &target, double t) {

  std::vector<std::vector<std::vector<double>>> outputs;

  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {

    Localvar localvarp                           = *iterator->second;
    std::vector<std::vector<double>> out_current = localvarp.values;
    outputs.push_back(out_current);
  }

  std::string output_string = "";
  for(size_t i = 0; i != outputs[0].size(); ++i) {
    for(size_t j = 0; j != outputs[0][i].size(); ++j) {
      output_string +=
          std::to_string(i) + "," + std::to_string(j) + "," + std::to_string(t);
      for(size_t iterator = 0; iterator != outputs.size(); iterator++) {
        output_string += "," + std::to_string(outputs[iterator][i][j]);
      }
      output_string += "\n";
    }
  }

  target << output_string;
};


// insert a new Localvar
void Localsvector2d::insert(std::pair<std::string, Localvar *> new_localvar) {

  contents.insert(new_localvar);
};


// delete the contents
void Localsvector2d::rm() {

  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {

    Localvar *localvarp = iterator->second;
    delete localvarp;
  }
};


// set all locals at a certain field to value
void Localsvector2d::set_field_at(std::pair<int, int> coords, double value) {

  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {
    std::string varname = iterator->first;
    set_variable_at(coords, value, varname);
  }
};


// set localvar name to value at coords
void Localsvector2d::set_variable_at(std::pair<int, int> coords, double value,
                                     std::string name) {

  Localvar *localvarp                            = contents[name];
  localvarp->values[coords.first][coords.second] = value;
};

// initialize
void Localsvector2d::initialize(Globalsvector &globals, Localsvector2d &locals,
                                Agentvector2d &agents, int t,
                                int t_scale){
  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {

    Localvar *localvarp = iterator->second;
    localvarp->initialize(globals, locals, agents, t, t_scale);
  }
};
