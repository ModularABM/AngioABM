#include "Globalsvector.h"
#include "Agentvector.h"

// Implementation of container class for global variables (name, value pairs)

// initialize empty
Globalsvector::Globalsvector() { contents = std::map<std::string, double>(); };


// initialize from a list of names and values.
Globalsvector::Globalsvector(std::vector<std::string> init_names,
                             std::vector<double> init_values) {
  // In case of different size use min of both sizes
  for(unsigned i = 0; i < std::min(init_names.size(), init_values.size());
      i++) {

    contents.insert(
        std::pair<std::string, double>(init_names[i], init_values[i]));
  }
};


// initialize from a vector of pairs string, double
Globalsvector::Globalsvector(
    std::vector<std::pair<std::string, double>> inits) {
  for(unsigned i = 0; i < inits.size(); i++) {

    contents.insert(inits[i]);
  }
};


// initialize from a map of string, double
Globalsvector::Globalsvector(std::map<std::string, double> inits) {
  for(auto iterator = inits.begin(); iterator != inits.end(); iterator++) {

    contents.insert(
        std::pair<std::string, double>(iterator->first, iterator->second));
  }
};


// empty placeholder for inheritance
void Globalsvector::update(Globalsvector &globals, Localsvector2d &locals,
                           Agentvector2d &agents, double t, int t_scale){};


// output for importing into R data.frame (x,y,t,type,varname, varvalue)
void Globalsvector::output_dt(std::ostream &target, double t) {
  target << t;
  for(auto iterator = contents.begin(); iterator != contents.end();
      iterator++) {

    target << "," << iterator->second;
  }
  target << "\n";
};


// empty placeholder
void Globalsvector::initialize(Globalsvector &globals, Localsvector2d &locals,
                               Agentvector2d &agents, double t, int t_scale){

};
