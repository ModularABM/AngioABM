////////////////////////////////////////////////////////////////////////////////
//
// Part of AngioABM, simulator for simple 2D agent based models
//
// Author:  Clemens Kühn
//
////////////////////////////////////////////////////////////////////////////////

#ifndef AGENT_H
#define AGENT_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include "Localsvector.h"
#include "Globalsvector.h"

// forward declrarations to resolve circ. dep. on updating
class Agentvector2d;
class Globalsvector;
class Localsvector2d;


// Agent class, parent for all other agents.
// Contains minimal info agent needs (positions, id), output and basic functions

class Agent {
public:
  int x, y, id;  // position and id of agent
  std::vector<int> neighbours;
  std::string
      type;  // to  check an agents_type before asking for internal variables
  std::unordered_map<std::string, double> v;
  std::unordered_map<std::string, double> p;

  // ___________________________________________________________________________
  // constructors:
  Agent(int x_val, int y_val, int id_val);
  Agent(int x_val, int y_val, int id_val, std::string type_val);
  Agent(int x_val, int y_val, int id_val, std::string type_val,
        std::unordered_map<std::string, double> *var_vals,
        std::unordered_map<std::string, double> *par_vals);


  // ___________________________________________________________________________
  // member functions:
  virtual ~Agent() {};             // virtual desctructor

  // move agent
  virtual void move(int x_step, int y_step);

  // set new position to newx, newy
  virtual void setpos(int x_new, int y_new);

  // assign new id
  void setid(int id_val);

  // for reading on screen
  virtual void output_state(std::ostream &target = std::cout);

  // for saving to disc and loading into R
  void output_dt(std::ostream &target = std::cout, double t = 0);

  // for saving compact
  void output_compact(std::ostream &target = std::cout, double t = 0,
                      std::vector<std::string> headers = {});

  // empty update-template
  virtual void update(Globalsvector &globals, Localsvector2d &locals,
                      Agentvector2d &oldstate, Agentvector2d &newstate,
                      double t, int t_scale, int t_intstep, std::mt19937 &rng);

  // get value of a specific localvariable in the neighbourhood.
  double sense_nb_localvar(Localsvector2d locals, std::string l_name, int x_pos,
                           int y_pos, int s_range);

  // subtract from localvariable in neighbourhood.
  void rem_nb_localvar(Localsvector2d &locals, std::string l_name, int x_pos,
                       int y_pos, double rem_val, int rem_range);

  // add to a localvariable in the neighbourhood
  void secrete_nb_localvar(Localsvector2d &locals, std::string l_name,
                           int x_pos, int y_pos, double sec_val);

  // turn towards a gradient
  std::pair<int, int> turn_up_gradient(Localsvector2d &localsvar,
                                       std::string l_name, int s_range,
                                       std::mt19937 &rng);

  // turn clock- or anticlockwise
  std::pair<int, int> turn(int x_step, int y_step, int clockwise);

  // turn randomlly
  std::pair<int, int> wiggle();

  // return value of varname in  neibhouring agnets.
  double sense_nb_variable(Agentvector2d agentset, std::string var_name);

  // substract a value from thy neighbours variable
  void rem_nb_variable(Agentvector2d agentset, std::string varname, double val);

  // initialize the Agent
  virtual void initialize(Globalsvector &globals, Localsvector2d &locals,
                          Agentvector2d &agents, double t, int t_scale);
};

#endif
