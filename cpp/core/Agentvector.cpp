#include "Agentvector.h"

// Implementation of Agentvector class


// create empty with just the max boundaries
Agentvector2d::Agentvector2d(int x_max, int y_max) {

  agents = std::map<int, Agent *>();
  coords =
      std::vector<std::vector<Agent *>>(x_max, std::vector<Agent *>(y_max));
  xy_max.first  = x_max;
  xy_max.second = y_max;

  par_dists = std::unordered_map<
      std::string, std::unordered_map<std::string, std::vector<double>>>();

  spec_dists = std::unordered_map<
      std::string, std::unordered_map<std::string, std::vector<double>>>();

  headers = std::vector<std::string>{};  //{"t,x,y,id,type"};
};


// get all ids from the Agentvector2d
std::vector<int> Agentvector2d::retrieve_ids() {
  std::vector<int> v;
  for(auto it = agents.begin(); it != agents.end(); ++it) {
    v.push_back(it->first);
  }
  return v;
};


// check if coordinate position is free
bool Agentvector2d::check_free(int x_check, int y_check) {

  // pair<int, int> qpos = pair<int, int>(x_check, y_check);
  // return (coords.count(qpos) == 0);
  return (coords[x_check][y_check] == NULL);
};


// check if coordinates are outside the xy_max
bool Agentvector2d::check_outside(int x_check, int y_check) {

  if(x_check < 0 or x_check > xy_max.first - 1 or y_check < 0 or
     y_check > xy_max.second - 1) {
    return true;
  }

  else {
    return false;
  }
};

// insert new distributions for an agents parameters
void Agentvector2d::insert_pardists(
    std::string agentname,
    std::unordered_map<std::string, std::vector<double>> parlist) {
  par_dists[agentname] = parlist;
};

// insert new distributions for an agents initial values
void Agentvector2d::insert_specdists(
    std::string agentname,
    std::unordered_map<std::string, std::vector<double>> speclist) {
  spec_dists[agentname] = speclist;
};


// update neighbourlists for some agent
void Agentvector2d::update_own_neighbours(int myid) {

  // get self
  Agent *self = agents.find(myid)->second;
  std::pair<int, int> ownpos = std::pair<int, int>(self->x, self->y);

  // find new neighbours by checking neighoburing positions
  std::vector<int> new_nbs = std::vector<int>();
  for(int i = std::max(0, ownpos.first - 1);
      i < std::min(xy_max.first, ownpos.first + 2); i++) {

    for(int j = std::max(0, ownpos.second - 1);
        j < std::min(xy_max.second, ownpos.second + 2); j++) {

      if(!(i == ownpos.first and j == ownpos.second)) {

        // pair<int, int> nb_pos = pair<int, int>(i, j);
        // if(coords.count(nb_pos) > 0) {
        if(coords[i][j]) {

          // Agent *nb_id = coords.find(nb_pos)->second;
          Agent *nb_id = coords[i][j];
          new_nbs.push_back(nb_id->id);
        }
      }
    }
  }
  self->neighbours = new_nbs;
};


// move agent and do all the necessary updating
int Agentvector2d::move_agent(int my_id, int x_step, int y_step) {

  // find self
  Agent *self              = agents.find(my_id)->second;
  std::vector<int> old_nbs = self->neighbours;
  // pair<int, int> my_pos  = pair<int, int>(self->x, self->y);
  std::pair<int, int> new_pos =
      std::pair<int, int>(self->x + x_step, self->y + y_step);

  int to_ret = 1;
  // move (or not)
  if(!check_outside(new_pos.first, new_pos.second)) {
    if(check_free(new_pos.first, new_pos.second)) {

      coords[self->x][self->y] = NULL;
      self->move(x_step, y_step);

      // remove from coords-list
      // unordered_map<pair<int, int>, Agent *,
      //               boost::hash<pair<int, int>>>::iterator it =
      //     coords.find(my_pos);
      // coords.erase(it);

      // add new position to coords-list
      // coords.insert(pair<pair<int, int>, Agent *>(new_pos, self));
      coords[self->x][self->y] = self;

      // update neighbours
      update_own_neighbours(my_id);
      update_old_neighbours(my_id, old_nbs);
      update_new_neighbours(my_id);
      to_ret = 0;
    }
  }

  return to_ret;
};


// remove self from old neighbourlist, e.g. after moving away
void Agentvector2d::update_old_neighbours(int my_id,
                                          std::vector<int> old_neighbours) {

  Agent *self = agents.find(my_id)->second;
  for(unsigned i = 0; i < old_neighbours.size(); i++) {

    Agent *nb = agents.find(old_neighbours[i])->second;
    if(nb->x > self->x + 1 or nb->x < self->x - 1 or nb->y > self->y + 1 or
       nb->y < self->y - 1) {

      nb->neighbours.erase(
          remove(nb->neighbours.begin(), nb->neighbours.end(), my_id),
          nb->neighbours.end());
    }
  }
};


// add self to all neighbours' neighbourlists if they don't know self
void Agentvector2d::update_new_neighbours(int my_id) {

  Agent self = *agents.find(my_id)->second;
  for(unsigned i = 0; i < self.neighbours.size(); i++) {

    bool found = false;
    Agent *nb  = agents.find(self.neighbours[i])->second;
    for(unsigned j = 0; j < nb->neighbours.size(); j++) {

      if(nb->neighbours[j] == my_id) {

        found = true;
        continue;
      }
    }
    if(found == false) {

      nb->neighbours.push_back(my_id);
    }
  }
};


// output for importing into R data.frame (x,y,t,type,varname, varvalue)
void Agentvector2d::output_dt(std::ostream &target, double timepoint) {
  for(auto it = agents.begin(); it != agents.end(); ++it) {

    it->second->output_dt(target, timepoint);
  }
};


// output compact (1 row per agent)
void Agentvector2d::output_compact(std::ostream &target, double timepoint) {
  // get all possible
  for(auto it = agents.begin(); it != agents.end(); ++it) {

    it->second->output_compact(target, timepoint, headers);
  }
};


// update by calling agents' update-fct. synchronicity, rand-order as parameters
void Agentvector2d::update(Globalsvector &globals, Localsvector2d &locals,
                           Agentvector2d &agentset, double timepoint,
                           int t_scale, int t_intstep, int sync, int randorder,
                           std::mt19937 &rng) {

  // std::cout << "\t\t\tupdating agents:\n";
  if(randorder == 1) {
    std::vector<int> agent_ids = retrieve_ids();
    shuffle(agent_ids.begin(), agent_ids.end(), rng);

    if(sync == 1) {

      Agentvector2d oldstate = Agentvector2d(agentset);
      for(unsigned i = 0; i < agent_ids.size(); i++) {
        agents[agent_ids[i]]->update(globals, locals, oldstate, agentset,
                                     timepoint, t_scale, t_intstep, rng);
      }
    } else {
      for(unsigned i = 0; i < agent_ids.size(); i++) {
        agents[agent_ids[i]]->update(globals, locals, agentset, agentset,
                                     timepoint, t_scale, t_intstep, rng);
      }
    }
  } else {
    if(sync == 1) {
      Agentvector2d oldstate    = Agentvector2d(agentset);
      std::vector<int> curr_ids = retrieve_ids();
      for(unsigned i = 0; i < curr_ids.size(); i++) {

        agents[curr_ids[i]]->update(globals, locals, oldstate, agentset,
                                    timepoint, t_scale, t_intstep, rng);
      }
    } else {
      std::vector<int> curr_ids = retrieve_ids();
      for(unsigned i = 0; i < curr_ids.size(); i++) {

        agents[curr_ids[i]]->update(globals, locals, agentset, agentset,
                                    timepoint, t_scale, t_intstep, rng);
      }
    }
  }
  // std::cout << "\t\t\t done updating agents\n";
};


// return size of agentvector
int Agentvector2d::size() {
  int size_val = 0;
  for(auto iterator = agents.begin(); iterator != agents.end(); iterator++) {
    size_val++;
  }
  return size_val;
};


// delete the contents
void Agentvector2d::rm() {
  for(auto iterator = agents.begin(); iterator != agents.end(); iterator++) {
    Agent *agentp = iterator->second;
    delete agentp;
  }
};


// get the total of a variable over all agents
double Agentvector2d::total_var(std::string v_name) {

  double tot = 0;
  for(auto iterator = agents.begin(); iterator != agents.end(); iterator++) {

    Agent *agentp = iterator->second;
    if(agentp->v.find(v_name) != agentp->v.end()) {

      tot += agentp->v[v_name];
    }
  }

  return tot;
};

// re-populate a unordered_map<string, double> based on distributions given
std::unordered_map<std::string, double> Agentvector2d::sample_set(
    std::unordered_map<std::string, double> existing,
    std::unordered_map<std::string, std::vector<double>> dists,
    std::mt19937 &rng) {

  // std::cout << "\t\t\tin sample_set\n";
  for(auto it = existing.begin(); it != existing.end(); ++it) {

    // std::cout << "\t\t\t\tworking on " << it->first << "\n";
    // std::cout << "\t\t\t\t\tand value is " << dists[it->first][0] << " \n";
    std::uniform_real_distribution<> dis(0, 1);

    if(dists[it->first][0] == 0.0) {
      // use uniform distribution
      std::uniform_real_distribution<> dis(dists[it->first][1],
                                           dists[it->first][0]);
      it->second = dis(rng);

    } else if(dists[it->first][0] == 1.0) {
      // use poisson distribution
      std::poisson_distribution<> dis(dists[it->first][1]);
      it->second = dis(rng);

    } else if(dists[it->first][0] == 2.0) {  // use exponential distribution
      std::exponential_distribution<> dis(dists[it->first][1]);
      it->second = dis(rng);
    } else if(dists[it->first][0] == 3.0) {
      // use normal distribution
      std::normal_distribution<> dis(dists[it->first][1], dists[it->first][2]);
      it->second = dis(rng);
    } else if(dists[it->first][0] == 4.0) {
      // use lognormal dist! LOGTRANSFORM PARAMETERS HERE!
      double m  = dists[it->first][1];
      double v  = dists[it->first][2];
      double mu = log(m / pow(1 + v / pow(m, 2), 0.5));
      double sd = m * pow(log(1 + v / pow(m, 2)), 1);
      std::lognormal_distribution<> dis(mu, sd);


      // std::lognormal_distribution<> dis(std::log(dists[it->first][1]),
      //                                   std::log(dists[it->first][2]));
      it->second = dis(rng);
      // std::cout << "\t\t\t\t\tparams are " << dists[it->first][1] << ", "
      //           << dists[it->first][2] << ", sampled value is " << it->second
      //           << "\n";
    } else if(dists[it->first][0] == 99.0) {
      // use value indicated
      it->second = dists[it->first][1];
    }
    // std::cout << "\t\t\t\tsampled on " << it->second << "\n";
  }
  return existing;
};

// re-populate a unordered_map<string, double> based on mean of value and dists
// given
std::unordered_map<std::string, double> Agentvector2d::samplemean_set(
    std::unordered_map<std::string, double> existing,
    std::unordered_map<std::string, std::vector<double>> dists,
    std::mt19937 &rng) {

  // std::cout << "\t\t\tin samplemean_set\n";

  for(auto it = existing.begin(); it != existing.end(); ++it) {
    // std::cout << "\t\t\t\tworking on " << it->first << "\n";
    // std::cout << "and value is " << dists[it->first][0] << " \n";

    std::uniform_real_distribution<> dis(0, 1);

    if(dists[it->first][0] == 0.0) {
      // use uniform distribution
      std::uniform_real_distribution<> dis(dists[it->first][1],
                                           dists[it->first][2]);
      it->second = (it->second + dis(rng)) / 2;
    } else if(dists[it->first][0] == 1.0) {
      // use poisson distribution
      std::poisson_distribution<> dis(dists[it->first][1]);
      it->second = (it->second + dis(rng)) / 2;
    } else if(dists[it->first][0] == 2.0) {
      // use exponential distribution
      std::exponential_distribution<> dis(dists[it->first][1]);
      it->second = (it->second + dis(rng)) / 2;
    } else if(dists[it->first][0] == 3.0) {
      // use normal distribution
      std::normal_distribution<> dis(dists[it->first][1], dists[it->first][2]);
      it->second = (it->second + dis(rng)) / 2;
    } else if(dists[it->first][0] == 4.0) {
      // use lognormal dist! LOGTRANSFORM PARAMETERS HERE!
      double m  = dists[it->first][1];
      double v  = dists[it->first][2];
      double mu = log(m / pow(1 + v / pow(m, 2), 0.5));
      double sd = m * pow(log(1 + v / pow(m, 2)), 1);
      std::lognormal_distribution<> dis(mu, sd);
      // std::lognormal_distribution<> dis(dists[it->first].second[0],
      //                                   dists[it->first].second[1]);
      it->second = (it->second + dis(rng)) / 2;
    } else if(dists[it->first][0] == 99.0) {
      // use value indicated
      it->second = dists[it->first][1];
    }
  }
  return existing;
};

void Agentvector2d::initialize(Globalsvector globals, Localsvector2d locals,
                               Agentvector2d agentset, double t, int t_scale){
  std::vector<int> agent_ids = retrieve_ids();
  for(unsigned i = 0; i < agent_ids.size(); i++) {
    agents[agent_ids[i]]->initialize(globals, locals, agentset, t, t_scale);
  }
};
